/**     
    * \file sae102.h
    * \author Erwan. M & Corentin. L
    * \date 11/12/22 
    * \brief Résumé : Fichier .c regroupant les fonctions du programme
*/

#include "sae102.h"
/**
 * \brief Charge le fichier iut.don et l'insère dans le tableau tiut
 * \param[in, out] tiut Tableau qui regroupe les iut
 * \param[in] tmax Taille logique du tableau tiut
 * \return int
 * 
 * Fonction qui insère les données présents dans le fichier iut.don dans le tableau tiut.
 * Elle retourne -1 si il y a eu un problème d'ouverture du fichier, -2 si le tableau est plein, sinon la taille logique.
 */

int chargerFichier(VilleIUT *tiut[], int tmax){

    int i = 0, trouve;

    VilleIUT vIUT;

    FILE *flot;

    flot=fopen("iut.don","r");

    if(flot == NULL){

        printf("Problème ouverture fichier.\n");

        return -1;

    }

    vIUT = lireVille(flot);

    while(!feof(flot)){

        if (i == tmax){

            printf("Tableaux pleins !\n");

            fclose(flot);

            return -2;

        }

        tiut[i] = (VilleIUT*) malloc (sizeof(VilleIUT));

        if (tiut[i] == NULL){

            printf("Problème malloc\n");

            fclose(flot);

            return -1;

        }

        trouve = insertionVille(tiut, vIUT, i);

        if (trouve == 0)

            i = i + 1;

        vIUT = lireVille(flot);

    }

    fclose(flot);

    printf("\033[1;32m"); // Vert

    printf("-------------------------------\n");

    // afficher(tiut, i);

    printf("Chargement fichier [iut.don] terminé.\n");

    printf("\033[0m"); // Reset couleur

    return i;

}

/**
 * \brief Charge le fichier candidats.bin et l'insère dans la liste de candidats
 * \param[in, out] liste Liste de candidats
 * \param[in, out] tlog Taille de la liste de candidats
 * \return ListeCandidats
 * 
 * Fonction qui insère les données présents dans le fichier candidats.bin dans la liste de candidats.
 * Elle quitte le programme si il y a eu un problème d'ouverture du fichier, sinon la liste.
 */

ListeCandidats chargerBinCandidats(ListeCandidats liste, int *tlog){

    Candidat c;

    FILE *flot;

    flot = fopen("candidats.bin","rb");

    if (flot == NULL){

        printf("Problème ouverture fichier [candidats.bin].\n");

        exit(1);

    }

    fread(tlog, sizeof(int), 1, flot);

    c = lireBinCandidat(flot);

    while(!feof(flot)){

        liste = insertionCandidat(liste, c);

        c = lireBinCandidat(flot);

    }

    fclose(flot);

    printf("\033[1;32m"); // Vert

    printf("-------------------------------\n");

    printf("Chargement fichier [candidats.bin] terminé.\n");

    printf("\033[0m"); // Reset couleur

    return liste;

}

/**
 * \brief Charge le fichier candidats.don et l'insère dans la liste de candidats
 * \param[in, out] liste Liste de candidats
 * \param[in, out] tlog Taille de la liste de candidats
 * \return ListeCandidats
 * 
 * Fonction qui insère les données présents dans le fichier candidats.don dans la liste de candidats.
 * Elle quitte le programme si il y a eu un problème d'ouverture du fichier, sinon la liste.
 */

ListeCandidats chargerTxtCandidats(ListeCandidats liste, int *tlog){

    Candidat c;

    int i;

    FILE *flot;

    flot = fopen("candidats.don","r");

    if (flot == NULL){

        printf("Problème ouverture fichier [candidats.don].\n");

        exit(1);

    }

    fscanf(flot, "%d", tlog);

    for (i = 0 ; i < *tlog ; i++){

        c = lireTxtCandidat(flot);
        
        liste = insertionCandidat(liste, c);

    }

    fclose(flot);

    printf("\033[1;32m"); // Vert

    printf("-------------------------------\n");

    printf("Chargement fichier [candidats.don] terminé.\n");

    printf("\033[0m"); // Reset couleur

    return liste;

}


/**
 * \brief Supprime l'élement en tête d'une liste de candidats
 * \param[in, out] liste Liste de candidats
 * \return ListeCandidats
 * 
 * Fonction qui supprime le candidat en tête de la liste.
 */

ListeCandidats supprimerEnTeteCandidats(ListeCandidats l){

    MaillonCandidat *aux;

    if (l == NULL){

        printf("Opération interdite\n");

        exit(1);

    }

    aux = l;

    l = l -> suiv;

    free(aux);

    return l;

}

/**
 * \brief Supprime l'élement en tête d'une liste de candidats
 * \param[in, out] liste Liste de candidats
 * \param[in] numCandid numéro du candidat
 * \return ListeCandidats
 * 
 * Fonction qui supprime le candidat entré en paramètres, elle fait de la récursivité pour chercher le candidat.
 */

ListeCandidats supprimerCandidats(ListeCandidats l, int numCandid){

    if (l == NULL) return l;

    if (numCandid == l -> c.numCandid) return supprimerEnTeteCandidats(l);

    l -> suiv = supprimerCandidats(l -> suiv, numCandid);

    return l;

}

/**
 * \brief Affiche un candidat
 * \param[in] c Candidat
 * \return void
 * 
 * Fonction qui affiche un candidat avec ses notes et ses choix. Elle ne retourne rien.
 */


void afficher1Candidat(Candidat c){

    printf("Numéro candidat : %d\nNom : %s\nPrénom : %s\n", c.numCandid, c.nom, c.prenom);

    printf("Maths : %.2f\tFrançais : %.2f\tAnglais : %.2f\tSpécialité : %.2f\n", c.notes[0], c.notes[1], c.notes[2], c.notes[3]);

    affichageChoix(c.choix);

}

/**
 * \brief Sauvegarde la liste de candidats dans un fichier binaire
 * \param[in] liste Liste de candidats
 * \param[in] tlog taille de la liste
 * \param[in] nomFichier Nom du fichier
 * \return void
 * 
 * Fonction qui enregistre la liste de candidats dans un fichier binaire. Elle ne retourne rien.
 */

void sauverCandidats(ListeCandidats liste, int tlog, char* nomFichier){

    FILE *flot;

    int longueur;

    flot = fopen(nomFichier, "wb");

    if (flot == NULL){

        printf("Problème lors de l'ouverture du fichier\n");

        return;

    }

    fwrite(&tlog, sizeof(int), 1, flot);

    while (liste != NULL){

        fwrite(&liste -> c.numCandid, sizeof(int), 1, flot);

        fwrite(liste -> c.nom, sizeof(char), 30, flot);

        fwrite(liste -> c.prenom, sizeof(char), 30, flot);

        fwrite(liste -> c.notes, sizeof(float), 4, flot);

        longueur = longueurListeChoix(liste -> c.choix);

        fwrite(&longueur, sizeof(int), 1, flot);

        sauverChoix(flot, liste -> c.choix);

        free(&liste -> c);

        liste = liste -> suiv;

    }

    fclose(flot);

}

/**
 * \brief Sauvegarde la liste de choix dans un fichier binaire
 * \param[in] liste Liste de choix du candidat
 * \param[in] flot Nom du fichier
 * \return void
 * 
 * Fonction qui enregistre les choix d'un candidat dans un fichier binaire, elle est liée avec la fonction sauverCandidats. Elle ne retourne rien.
 */

void sauverChoix(FILE *flot, ListeChoix liste){

    if (liste == NULL){

        return;

    }

    fwrite(liste -> c.ville, sizeof(char), 30, flot);

    fwrite(liste -> c.departement, sizeof(char), 30, flot);

    fwrite(&(liste -> c).decision, sizeof(int), 1, flot);

    fwrite(&(liste -> c).validation, sizeof(int), 1, flot);

    sauverChoix(flot, liste -> suiv);

    free(&liste -> c);

}

/**
 * \brief Lit un candidat dans un fichier binaire
 * \param[in] flot Nom du fichier
 * \return Candidat
 * 
 * Fonction qui lit un candidat dans un fichier binaire. Elle retourne un candidat.
 */

Candidat lireBinCandidat(FILE* flot){

    Candidat c;

    int nbChoix;

    fread(&c.numCandid, sizeof(int), 1, flot);

    fread(c.nom, sizeof(char), 30, flot);

    fread(c.prenom, sizeof(char), 30, flot);

    fread(c.notes, sizeof(float), 4, flot);
    
    fread(&nbChoix, sizeof(int), 1, flot);

    c.choix = lireBinChoix(flot, nbChoix);

    return c;

}

/**
 * \brief Lit un candidat dans un fichier texte
 * \param[in] flot Nom du fichier
 * \return Candidat
 * 
 * Fonction qui lit un candidat dans un fichier texte. Elle retourne un candidat.
 */

Candidat lireTxtCandidat(FILE* flot){

    Candidat c;

    int nbChoix, i;

    fscanf(flot, "%d%*c", &c.numCandid);

    fgets(c.nom, 28, flot);

    c.nom[strlen(c.nom) - 1] = '\0';

    fgets(c.prenom, 28, flot);

    c.prenom[strlen(c.prenom) - 1] = '\0';

    for (i = 0 ; i < 4 ; i++)

        fscanf(flot, "%f", &c.notes[i]);

    fscanf(flot, "%d", &nbChoix);

    c.choix = lireTxtChoix(flot, nbChoix);

    return c;

}

/**
 * \brief Lit les choix d'un candidat dans un fichier binaire
 * \param[in] flot Nom du fichier
 * \param[in] nbChoix Nombre de choix du candidat
 * \return ListeChoix
 * 
 * Fonction qui lit les choix d'un candidat dans un fichier binaire. Elle retourne une liste de choix.
 */

ListeChoix lireBinChoix(FILE* flot, int nbChoix){

    ListeChoix liste;

    liste = listeChoixNouv();

    int i;

    Choix choix;

    for (i = 0 ; i < nbChoix ; i++){

        fread(choix.ville, sizeof(char), 30, flot);

        fread(choix.departement, sizeof(char), 30, flot);

        fread(&choix.decision, sizeof(int), 1, flot);

        fread(&choix.validation, sizeof(int), 1, flot);

        liste = ajouterChoix(liste, choix);

    }

    return liste;

}

/**
 * \brief Lit les choix d'un candidat dans un fichier texte
 * \param[in] flot Nom du fichier
 * \param[in] nbChoix Nombre de choix du candidat
 * \return ListeChoix
 * 
 * Fonction qui lit les choix d'un candidat dans un fichier texte. Elle retourne une liste de choix.
 */

ListeChoix lireTxtChoix(FILE* flot, int nbChoix){

    ListeChoix liste;

    liste = listeChoixNouv();

    int i;

    Choix choix;

    for (i = 0 ; i < nbChoix ; i++){

        fscanf(flot, "%s", choix.ville);

        fscanf(flot, "%s", choix.departement);

        fscanf(flot, "%d", &choix.decision);

        fscanf(flot, "%d", &choix.validation);

        liste = ajouterChoix(liste, choix);

    }

    return liste;

}

/**
 * \brief Ajoute un choix dans la liste de choix du candidat
 * \param[in] liste Liste des choix du candidat
 * \param[in] choix choix à insérer
 * \return ListeChoix
 * 
 * Fonction qui ajoute un choix dans la liste de choix du candidat. Elle quitte le programme si il y a un problème malloc, sinon elle retourne une liste de choix.
 */

ListeChoix ajouterChoix(ListeChoix liste, Choix choix){

    MaillonChoix *m;

    m = (MaillonChoix*) malloc (sizeof(MaillonChoix));

    if (m == NULL){

        printf("Problème malloc\n");

        exit(1);

    }

    m -> c = choix;

    m -> suiv = liste; //ajoute en tête
    
    return m;

}

/**
 * \brief Insère un candidat dans la liste de candidats
 * \param[in] liste Liste des choix du candidat
 * \param[in] c candidat à insérer
 * \return ListeCandidats
 * 
 * Fonction qui ajoute un candidat dans la liste de candidats. Elle quitte le programme si il y a un problème malloc, sinon elle retourne une liste de candidats.
 */

ListeCandidats insertionCandidat(ListeCandidats liste, Candidat c){

    MaillonCandidat *m;

    m = (MaillonCandidat*) malloc (sizeof(MaillonCandidat));

    if (m == NULL){

        printf("Problème malloc\n");

        exit(1);

    }

    m -> c = c;

    m -> suiv = liste;

    return m;

}

/**
 * \brief Initialise une liste de candidats
 * \return ListeCandidats
 * 
 * Fonction qui initialise une liste de candidats. Elle retourne donc une liste de candidats.
 */

ListeCandidats listeCandidatsNouv(){

    ListeCandidats l;

    l = NULL;

    return l;

}

/**
 * \brief Initialise une liste de choix
 * \return ListeChoix
 * 
 * Fonction qui initialise une liste de choix. Elle retourne donc une liste de choix.
 */

ListeChoix listeChoixNouv(){

    ListeChoix l;

    l = NULL;

    return l;

}

/**
 * \brief Affiche les candidats dans la liste
 * \param[in] liste Liste des candidats
 * \return void
 * 
 * Fonction qui affiche les candidats dans la liste de candidats, avec leurs notes et leurs choix. Ne retourne rien.
 */


void affichageCandidats(ListeCandidats liste){

    if (liste == NULL)

        return;

    printf("Numéro candidat : %d\nNom : %s\nPrénom : %s\n", liste -> c.numCandid, liste -> c.nom, liste -> c.prenom);

    printf("Maths : %.2f\tFrançais : %.2f\tAnglais : %.2f\tSpécialité : %.2f\n", liste -> c.notes[0], liste -> c.notes[1], liste -> c.notes[2], liste -> c.notes[3]);

    affichageChoix(liste -> c.choix);

    affichageCandidats(liste -> suiv);

}

/**
 * \brief Affiche les choix dans la liste
 * \param[in] liste Liste des choix
 * \return void
 * 
 * Fonction qui affiche les choix des candidats. Ne retourne rien.
 */

void affichageChoix(ListeChoix liste){

    if (liste == NULL){

        return;

    }

    printf("Ville : %s\tDépartement : %s\tDécision : %d\tValidation : %d\n", liste -> c.ville, liste -> c.departement, liste -> c.decision, liste -> c.validation);

    affichageChoix(liste -> suiv);

}

/**
 * \brief Insère une ville dans tiut
 * \param[in, out] tiut Tableau des iut
 * \param[in] vIUT Ville à insérer
 * \param[in] tlog taille de tiut
 * \return int
 * 
 * Fonction qui insère une ville dans le teableau tiut. Elle quitte le programme si il y a un problème malloc, sinon elle retourne trouve (si la ville existe déjà ou non).
 */

int insertionVille(VilleIUT* tiut[], VilleIUT vIUT, int tlog){

    int position, trouve;

    position = rechercheVille(tiut, vIUT.ville, tlog, &trouve);

    if (trouve == 0){ 

        decalageADroite(tiut, tlog, position);

        tiut[position] = (VilleIUT *) malloc (sizeof(VilleIUT));

        if (tiut[position] == NULL){

            printf("Problème malloc\n");

            exit(1);

        }
        
        strcpy(tiut[position] -> ville, vIUT.ville);

        tiut[position] -> ldept = inserer(tiut[position] -> ldept, vIUT.ldept -> departement, vIUT.ldept -> nbP, vIUT.ldept -> nomResp);

    } 

    else

        tiut[position] -> ldept = inserer(tiut[position] -> ldept, vIUT.ldept -> departement, vIUT.ldept -> nbP, vIUT.ldept -> nomResp);

    return trouve;

}

/**
 * \brief Décale à droite tiut
 * \param[in, out] tiut Tableau des iuts
 * \param[in] tlog Taille de tiut
 * \param[in] index Limite du décalage
 * \return void
 * 
 * Fonction qui décale à droite les éléments du tableau tiut. Elle ne retourne rien.
 */

void decalageADroite(VilleIUT *tiut[], int tlog, int index){

    int i;

    for (i = tlog; i > index ; i--)

        tiut[i] = tiut[i - 1];  

}

/**
 * \brief Recherche une ville dans tiut
 * \param[in, out] tiut Tableau des iuts
 * \param[in, out] ville Ville à rechercher
 * \param[in] tlog Taille de tiut
 * \param[in, out] trouve Si la ville est trouvée ou non
 * \return int
 * 
 * Fonction qui fait une recherche dichotomique pour trouver une ville entrée en paramètres. Elle retourne l'emplacement de cette dernière.
 */

int rechercheVille(VilleIUT** tiut, char ville[], int tlog, int *trouve){

    int sup = tlog-1, inf = 0, m;

    while(sup >= inf){

        m = (sup + inf) / 2;

        if (strcmp(tiut[m] -> ville, ville) == 0){

            *trouve = 1;

            return m;

        }

        if (strcmp(tiut[m] -> ville, ville) > 0)

            sup = m - 1;

        else

            inf = m + 1;
    
    }

    *trouve = 0;

    return inf;

}

/**
 * \brief Calcule une note pour chaque candidats
 * \param[in] liste Liste des candidats
 * \param[in] tlog Taille de la liste
 * \return void
 * 
 * Fonction qui calcule une note moyenne pour les candidats et créer ensuite un fichier. Elle ne retourne rien.
 */

void attributionNote(ListeCandidats liste, int tlog){

    int res;

    res = ecritureNoteMoyenne(liste, tlog);

    if(res == -1) return ;

    printf("\033[1;32m"); // Vert

    printf("-------------------------------\n");

    printf("Chargement fichier [notesCandidats.don] terminé.\n");

    printf("\033[0m"); // Reset couleur

}

/**
 * \brief Écrit dans un fichier une note moyenne pour les candidats
 * \param[in] liste Liste des candidats
 * \param[in] tlog Taille de la liste
 * \return int
 * 
 * Fonction qui créer un fichier qui regroupe les notes moyennes des candidats, elle est liée à la fonction attributionNote. Elle retourne -1 si il y a un problème d'ouverture, sinon 0.
 */

int ecritureNoteMoyenne(ListeCandidats liste, int tlog){

    int i;

    float noteMoyenne;

    FILE* flot;

    flot = fopen("notesCandidats.don", "w");

    if(flot==NULL){

        printf("Problème écriture fichier [notesCandidats.don]\n");

        return -1;

    }

    for(i = 0 ; i < tlog ; i++) {

        noteMoyenne = (liste -> c.notes[0] * 2 + liste -> c.notes[1] + liste -> c.notes[2] * 2 + liste -> c.notes[3] * 4) / 9;

        fprintf(flot, "%d\n%.2f\n", liste->c.numCandid, noteMoyenne);

        liste=liste->suiv;

    }

    fclose(flot);

    return 0;

}

/**
 * \brief Recherche un département dans une ville
 * \param[in, out] tiut Tableau des iut
 * \param[in] ville Ville à rechercher
 * \param[in] departement Département à rechercher
 * \param[in] tlog Taille de tiut
 * \param[in, out] trouve Si le département est trouvé ou non.
 * \return int
 * 
 * Fonction qui recherche un département dans une ville, elle est liée à la fonction rechercheVille et rechercheDept. Elle retourne -1 si le département n'est pas trouvé, sinon son emplacement dans la liste de département.
 */

int rechercheDepartement(VilleIUT* tiut[], char ville[], char departement[], int tlog, int *trouve){

    int posville, compteur = 0, trouve2 = 0;

    posville = rechercheVille(tiut, ville, tlog, &trouve2);

    if (trouve2 == 1)

        return rechercheDept (tiut[posville] -> ldept, departement, trouve, compteur);

    printf("Ville introuvable ! Réessayez !\n");

    return -1;

}

/**
 * \brief Recherche un département dans une ville trouvée 
 * \param[in] l Liste des départements
 * \param[in] departement Departement à rechercher
 * \param[in] trouve Si le département est trouvé ou non
 * \param[in] compteur Emplacement du département dans la liste
 * \return int
 * 
 * Fonction qui recherche un département dans une ville trouvée précedemment, elle est liée à la fonction rechercheDepartement. Elle retourne son emplacement dans la liste de département.
 */

int rechercheDept(ListeDept l, char departement[], int *trouve, int compteur){

    if (l == NULL){

        *trouve = 0;

        return compteur;

    }

    if (strcmp(departement, l -> departement) < 0){

        *trouve = 0;

        return compteur;

    }

    if (strcmp(departement, l -> departement) == 0){

        *trouve = 1;

        return compteur;

    }

    return rechercheDept(l -> suiv, departement, trouve, compteur + 1);

}

/**
 * \brief Calcule la longueur d'une liste de choix
 * \param[in] liste Liste des choix
 * \return int
 * 
 * Fonction qui calcule la longueur d'une liste de choix. Elle retourne sa longueur.
 */

int longueurListeChoix(ListeChoix liste){
    
    if (liste == NULL) return 0;

    return 1 + longueurListeChoix(liste -> suiv);

}

/**
 * \brief Calcule la longueur d'une liste de candidats
 * \param[in] liste Liste des candidats
 * \return int
 * 
 * Fonction qui calcule la longueur d'une liste de candidats. Elle retourne sa longueur.
 */

int longueurListeCandidats(ListeCandidats liste){

    if (liste == NULL) return 0;

    return 1 + longueurListeCandidats(liste -> suiv);

}

/**
 * \brief Lit une ville dans tiut
 * \param[in] flot Nom du fichier
 * \return VilleIUT
 * 
 * Fonction qui lit une ville avec ses informations dans un fichier donné. Elle quitte le programme si il y a un problème malloc, sinon elle retourne la ville.
 */

VilleIUT lireVille(FILE* flot){

    VilleIUT vIUT;

    vIUT.ldept = (ListeDept)malloc(sizeof(MaillonDept));

    if (vIUT.ldept == NULL){

        printf("Problème malloc\n");

        exit(1);

    }

    fscanf(flot, "%s %s %d", vIUT.ville, vIUT.ldept -> departement, &vIUT.ldept -> nbP);

    fgets(vIUT.ldept -> nomResp, 22, flot);

    vIUT.ldept -> nomResp[strlen(vIUT.ldept -> nomResp) - 1] = '\0';

    // printf("%s\t%s\t%d\t%s\n", vIUT.ville, vIUT.ldept->departement, vIUT.ldept->nbP, vIUT.ldept->nomResp);

    return vIUT;

}

/**
 * \brief Sauvegarde tiut dans iut.don
 * \param[in] tiut Tableau des iut
 * \return void
 * 
 * Fonction qui sauvegarde le tableau tiut dans le fichier iut.don. Elle quitte le programme si il y a un problème d'ouverture. Elle ne retourne rien.
 */

void sauvegarder(VilleIUT *tiut[], int tlog){

    int i;

    FILE* flot;

    flot = fopen("iut.don", "w");

    if (flot == NULL){

        printf("Problème lors de l'ouverture du fichier binaire en écriture\n");

        exit(1);

    }

    for (i = 0; i < tlog ; i++){

        sauvegarderListe(tiut[i] -> ldept, flot, tiut[i] -> ville);

        free(tiut[i] -> ldept);

        free(tiut[i]);

    }

    fclose(flot);

}

/**
 * \brief Sauvegarde la liste des départements dans iut.don
 * \param[in] l Liste des départements
 * \param[in] flot Nom du fichier
 * \param[in] ville Ville de l'iut
 * \return void
 * 
 * Fonction qui sauvegarde la liste des départements dans le fichier iut.don. Elle est liée à la fonction sauvegarder. Elle ne retourne rien.
 */

void sauvegarderListe(ListeDept l, FILE* flot, char* ville){

    if (l == NULL)

        return;

    fprintf(flot, "%s ", ville);

    fprintf(flot, "%s %d %s\n", l -> departement, l -> nbP, l -> nomResp);

    sauvegarderListe(l -> suiv, flot, ville);

}

/**
 * \brief Affiche le tableau tiut
 * \param[in] tiut Tableau des iut
 * \param[in] tlog Taille de tiut
 * \return void
 * 
 * Fonction qui affiche les informations du tableau tiut. Elle est liée à la fonction afficherListe. Elle ne retourne rien.
 */

void afficher(VilleIUT* tiut[], int tlog){

    int i;

    for (i = 0 ; i < tlog ; i++){

        printf("%s\t", tiut[i] -> ville);

        afficherListe(tiut[i] -> ldept);

    }

}

/**
 * \brief Affiche la liste de départements
 * \param[in] l Liste des départements
 * \return void
 * 
 * Fonction qui affiche les départements de la liste entrée en paramètres. Elle est liée à la fonction afficher. Elle ne retourne rien.
 */

void afficherListe(ListeDept l){

	if (l == NULL){

        printf("\n");

        return;

    }

	printf("%s\t%d\t%s\t|\t", l -> departement, l -> nbP, l -> nomResp);

	afficherListe(l -> suiv);

}

/**
 * \brief Lit le fichier phase.don
 * \param[in] void
 * \return int
 * 
 * Fonction qui lit le fichier phase.don pour savoir la phase en cours. Elle retourne l'état de la phase (0:Inactif, 1:Actif).
 */

int phaseCandid(void){

    int etat;

    FILE* flot;

    flot=fopen("phase.don", "r");

    if (flot == NULL){

        printf("Problème ouverture fichier phase.\n");

        return -1;
    }

    fscanf(flot, "%d", &etat);

    fclose(flot);

    return etat;

}

/**
 * \brief Switch pour la phase de candidature
 * \param[in] etat État de la phase
 * \return int
 * 
 * Fonction qui fait un switch pour changer la phase de candidature. Elle ne retourne rien.
 */

void switchPhase(int *etat){

    int choix;

    clear();

    switch (*etat){

        case 1:

            printf("Etat actuel : Actif\n");

            printf("Voulez-vous arrêter la phase de candidature ?\n\n");

            printf("(0) Non\n");

            printf("(1) Oui\n");

            choix=fonctionChoix();

            if(choix == 1){

                modifFichierPhase(0);

                *etat = 0;

                clear();

                printf("Phase de candidature arrêtée !\n");

                break;

            }

            else if(choix == 0){

                clear();

                printf("Modification annulée.\n");

                break;

            }

            affichageErreurSaisie();

            break;

        case 0:

            printf("Etat actuel : Inactif\n");

            printf("Voulez-vous lancer la phase de candidature ?\n\n");

            printf("(0) Non\n");

            printf("(1) Oui\n");

            choix=fonctionChoix();

            if(choix == 1){

                modifFichierPhase(1);

                *etat = 1;

                clear();

                printf("Phase de candidature lancée !\n");

                break;

            }

            else if(choix == 0){

                clear();

                printf("Modification annulée.\n");

                break;

            }

            affichageErreurSaisie();

            break;

        default:

            break;

    }

}

/**
 * \brief Change l'état de la phase
 * \param[in] etat État de la phase
 * \return int
 * 
 * Fonction qui change la phase de candidature et l'écrit dans le fichier phase.don. Elle ne retourne rien.
 */

void modifFichierPhase(int etat){

    FILE *flot;

    flot=fopen("phase.don", "w");

    fprintf(flot, "%d", etat);

    fclose(flot);

}

/**
 * \brief Switch pour le menu administrateur
 * \param[in] choix choix de l'utilisateur
 * \param[in] tiut Tableau des iuts
 * \param[in] tlog Taille de tiut
 * \param[in] etatPhase État de la phase
 * \param[in] liste Liste des candidats
 * \return void
 * 
 * Fonction qui fait un switch pour le menu administrateur. Elle ne retourne rien.
 */

void switchAdministrateur(int choix, VilleIUT** tiut, int tlog, int *etatPhase, ListeCandidats liste){

    char departement[22], ville[32], nomResp[22], nomCandid[30], prenomCandid[30], nomDep[30], nomVille[30];

    int trouve, trouve2, res, res2, i, position, positionTab, nbP;

    switch (choix){

        case 1:

            clear();

            res2 = saisieVille(ville, tiut, tlog);

            if (res2 == -1){

                printf("Saisie annulée\n");

                return;

            }

            clear();

            res = saisieDept(departement);

            if (res == -1){

                printf("Saisie annulée\n");

                return;

            }

            positionTab = rechercheVille(tiut, ville, tlog, &trouve);

            position = rechercheDepartement(tiut, ville, departement, tlog, &trouve);

            if (trouve == 1){

                nbP = saisieNbPlace();

                modifNbPlaces(tiut, positionTab, position, nbP);

            }

            else{

                clear();

                printf("-------------------------------\n");

                printf("\033[1;31m"); // Rouge

                printf("Erreur : Département inexistant !\n");

                printf("\033[0m"); // Reset couleur
                
            }

            break;

        case 2:

            res2 = saisieVille(ville, tiut, tlog);

            if (res2 == -1){

                printf("Saisie annulée !\n");

                return;

            }

            res = saisieDept(departement);

            if (res == -1){

                printf("Saisie annulée\n");

                return;

            }

            positionTab = rechercheVille(tiut, ville, tlog, &trouve);

            position = rechercheDepartement(tiut, ville, departement, tlog, &trouve2);

            if (trouve == 1 && trouve2 == 0){

                nbP = saisieNbPlace();

                scanf("%*c");

                res2 = saisieNvResp(nomResp);

                if (nbP == -1 || res2 == -1){

                    printf("Saisie annulée\n");

                    return;

                }

                tiut[positionTab] -> ldept = inserer(tiut[positionTab] -> ldept, departement, nbP, nomResp);

            }

            else if (trouve == 1 && trouve2 == 1){

                clear();

                printf("Département déjà existant !\n");

            }

            else {

                clear();

                printf("-------------------------------\n");

                printf("\033[1;31m"); // Rouge

                printf("Erreur : Ville inexistante !\n");

                printf("\033[0m"); // Reset couleur

            }

            break;

        case 3:

            res2 = saisieVille(ville, tiut, tlog);

            if (res2 == -1){

                printf("Saisie annulée\n");

                return;

            }

            res = saisieDept(departement);

            if (res == -1){

                printf("Saisie annulée\n");

                return;

            }

            positionTab = rechercheVille(tiut, ville, tlog, &trouve);

            position = rechercheDepartement(tiut, ville, departement, tlog, &trouve2);

            if (trouve == 1 && trouve2 == 1){

                tiut[positionTab] -> ldept = supprimer(tiut[positionTab] -> ldept, departement);

                if (tiut[positionTab] -> ldept == NULL)

                    for (i = positionTab; i < tlog - 1 ; i++)

                        tiut[i] = tiut[i + 1];

            }

            else{

                clear();

                printf("-------------------------------\n");

                printf("\033[1;31m"); // Rouge

                printf("Erreur : Département ou ville inexistant(e) !\n");

                printf("\033[0m"); // Reset couleur

            }

            break;

        case 4:

            clear();

            *etatPhase = phaseCandid();

            switchPhase(etatPhase);

            break;

        case 5:

            res2 = saisieVille(ville, tiut, tlog);

            if (res2 == -1){

                printf("Saisie annulée\n");

                return;

            }

            res = saisieDept(departement);

            if (res == -1){

                printf("Saisie annulée\n");

                return;

            }

            positionTab = rechercheVille(tiut, ville, tlog, &trouve);

            position = rechercheDepartement(tiut, ville, departement, tlog, &trouve);

            if (trouve == 1){

                res = saisieNvResp(nomResp);

                modifResp(tiut, positionTab, position, nomResp);

            }

            else{

                clear();

                printf("-------------------------------\n");

                printf("\033[1;31m"); // Rouge

                printf("Erreur : Département inexistant !\n");

                printf("\033[0m"); // Reset couleur

            }

            break;

        case 6:

            clear();

            printf("-------------------------------\n\n");

            afficher(tiut, tlog);

            break;

        case 7:

                clear();

                scanf("%*c");

                printf("Nom du candidat : ");

                fgets(nomCandid, 30, stdin);

                nomCandid[strlen(nomCandid)-1] = '\0';

                printf("Prénom du candidat : ");

                fgets(prenomCandid, 30, stdin);

                printf("-------------------------------\n");

                prenomCandid[strlen(prenomCandid)-1] = '\0';

                rechercheCandidat(liste, nomCandid, prenomCandid, &trouve);

                printf("\033[1;31m"); // Rouge

                if(trouve == -1) printf("Erreur : Candidat inexistant !\n");

                printf("\033[0m"); // Reset couleur

                break;

            case 8:

                clear();

                scanf("%*c");

                printf("Nom de la ville : ");

                fgets(nomVille, 30, stdin);

                nomVille[strlen(nomVille)-1] = '\0';

                printf("Nom du département : ");

                fgets(nomDep, 30, stdin);

                printf("-------------------------------\n");

                nomDep[strlen(nomDep)-1] = '\0';

                rechercheDeptTousCandidat(liste, nomVille, nomDep);

                printf("\033[1;31m"); // Rouge

                rechercheDepartement(tiut, nomVille, nomDep, tlog, &trouve);

                if(trouve == 0) printf("Erreur : Département inexistant !\n");

                printf("\033[0m"); // Reset couleur

                break;

        case 9:

            clear();

            return;
        
        default:

            affichageErreurSaisie();
    }
}

/**
 * \brief Recherche les candidats qui ont une candidature pour un département en particulier
 * \param[in] liste Liste des candidats
 * \param[in] nomVille Nom de la ville
 * \param[in] nomDep Nom du département
 * \return void
 * 
 * Fonction qui recherche les candidats qui ont une candidature pour un département en particulier. Elle ne retourne rien.
 */

void rechercheDeptTousCandidat(ListeCandidats liste, char* nomVille, char* nomDep){

    while(liste !=NULL){

        rechercheDeptCandidatE(liste->c.choix, nomVille, nomDep, liste);

        liste = liste -> suiv;

    }

}

/**
 * \brief Affichage du menu principal
 * \param[in] void
 * \return void
 * 
 * Fonction qui affiche le menu principal. Elle ne retourne rien.
 */

void affichageMenuPrincipal(void){

    printf("-------------------------------\n");
        
    printf("\033[1;37m"); // Gras

    printf("\tMENU PRINCIPAL\t\n");

    printf("\033[0m"); // Reset couleur
    
    printf("-------------------------------\n\n");
    
    printf("(1) Utilisateur\n");
    
    printf("(2) Administrateur\n");
    
    printf("\n(9) Quitter\n");

}
/**
 * \brief Affichage du menu principal + utilisateur responsable
 * \param[in] void
 * \return void
 * 
 * Fonction qui affiche le menu principal et l'utilisateur responsable. Elle ne retourne rien.
 */

void affichageMenuPrincipal2(void){

    printf("-------------------------------\n");
        
    printf("\033[1;37m"); // Gras

    printf("\tMENU PRINCIPAL\t\n");

    printf("\033[0m"); // Reset couleur
    
    printf("-------------------------------\n\n");
    
    printf("(1) Utilisateur\n");
    
    printf("(2) Administrateur\n");

    printf("(3) Responsable\n");
    
    printf("\n(9) Quitter\n");

}

/**
 * \brief Switch pour le menu principal
 * \param[in] choix Choix de l'utilisateur
 * \param[in] tiut Tableau des iut
 * \param[in] tlog Taille de tiut
 * \param[in, out] etatPhase État actuel de la phase
 * \param[in] liste Liste des candidats
 * \param[in, out] admis Liste des candidats admis
 * \param[in, out] attente Liste des candidats en attente
 * \return void
 * 
 * Fonction qui fait un switch pour le menu principal. Elle ne retourne rien.
 */

void switchPrincipal(int choix, VilleIUT **tiut, int tlog, int *etatPhase, ListeCandidats liste, ListeCandidats *admis, File *attente){

    switch (choix){

        case 1:

            clear();

            sousMenuUtilisateur(tiut, tlog, liste, admis, attente, etatPhase);

            break;

        case 2:

            clear();

            sousMenuAdministrateur(tiut, tlog, etatPhase, liste); // Sous Menu Administrateur

            break;

        case 3:

            clear();

            if (*etatPhase == 0)

                affichageErreurSaisie();
            
            else

                sousMenuResponsable(liste, admis, attente);

            break;

        case 9:

            clear();

            return;
            
        default:

            affichageErreurSaisie();  
    }
}

/**
 * \brief Menu responsable
 * \param[in] liste Liste des candidats
 * \param[in, out] admis Liste des candidats admis
 * \param[in, out] attente Liste des candidats en attente
 * \return void
 * 
 * Fonction qui gère le menu responsable. Elle ne retourne rien.
 */

void sousMenuResponsable(ListeCandidats liste, ListeCandidats *admis, File *attente){

    int choix;

    char motDePasseResponsable[25];

    printf("Mot de passe responsable : ");

    scanf("%*c");

    fgets(motDePasseResponsable, 25, stdin);

    motDePasseResponsable[strlen(motDePasseResponsable)-1] = '\0';

    if(strcmp("Je suis responsable", motDePasseResponsable) != 0){

        affichageErreurMdp();

        return;

    }

    clear();

    while(choix != 9){

        affichageSousMenuResponsable();

        choix = fonctionChoix();

        switchResponsable(choix, liste, admis, attente); 
    
    }

}

/**
 * \brief Switch pour le menu responsable
 * \param[in] choix Choix de l'utilisateur
 * \param[in] liste Liste des candidats
 * \param[in, out] admis Liste des candidats admis
 * \param[in, out] attente Liste des candidats en attente
 * \return void
 * 
 * Fonction qui fait un switch pour le menu responsable. Elle ne retourne rien.
 */

void switchResponsable(int choix, ListeCandidats liste, ListeCandidats *admis, File *attente){

    int maxAdmis, tabNumCandidat[30];

    int tlog3, i, trouve, res, trouveVoeu, trouveCandidat;

    float noteMin, tabMoyennes[30];

    char nom[30], prenom[30], ville[30], departement[22];

    ListeCandidats listeCandidatsClermont, save = listeCandidatsNouv();

    switch (choix){

        case 1:

            clear();

            listeCandidatsClermont = rechercheVoeux(liste);

            affichageCandidats(listeCandidatsClermont);

            break;

        case 2:

            clear();

            listeCandidatsClermont = rechercheVoeux(liste);

            save = listeCandidatsClermont;

            listeCandidatsClermont = triFusion(listeCandidatsClermont);

            affichageCandidats(listeCandidatsClermont);

            listeCandidatsClermont=save;

            break;

        case 3:

            clear();

            tlog3=lectureMoyennes(tabNumCandidat, tabMoyennes);

            for(i=0;i<tlog3;i++) printf("Numéro candidat : %d\nMoyenne : %.2f\n", tabNumCandidat[i], tabMoyennes[i]);

            break;

        case 4:

            clear();

            printf("Note minimum : ");

            scanf("%f",&noteMin);

            clear();

            printf("Maximum admis : ");

            scanf("%d",&maxAdmis);

            tlog3=lectureMoyennes(tabNumCandidat, tabMoyennes);

            clear();

            gestionCandidats(liste, tlog3, noteMin, maxAdmis);

            break;

        case 5:

            clear();

            printf("Note minimum : ");

            scanf("%f",&noteMin);

            clear();

            printf("Maximum admis : ");

            scanf("%d",&maxAdmis);

            tlog3 = lectureMoyennes(tabNumCandidat, tabMoyennes);

            clear();

            liste = changeDecision(liste, tlog3, noteMin, maxAdmis, admis, attente);

            break;

        case 6:

            clear();

            printf("- Liste des candidats admis : - \n\n");

            affichageCandidats(*admis);

            printf("\n- File des candidats en attente : - \n\n");

            afficherFile(*attente);

            break;

        case 7:

            clear();

            scanf("%*c");

            printf("Nom du candidat : ");

            fgets(nom, 30, stdin);

            nom[strlen(nom)-1] = '\0';

            printf("Prénom du candidat : ");

            fgets(prenom, 30, stdin);

            prenom[strlen(prenom)-1] = '\0';

            printf("-------------------------------\n");

            rechercheCandidat(liste, nom, prenom, &trouve);

            printf("\033[1;31m"); // Rouge

            if(trouve == -1) printf("Erreur : Candidat non trouvé !\n");

            printf("\033[0m"); // Reset couleur

            break;

        case 8:

            clear();

            scanf("%*c");

            printf("Nom du candidat : ");

            fgets(nom, 30, stdin);

            nom[strlen(nom)-1] = '\0';

            printf("Prénom du candidat : ");

            fgets(prenom, 30, stdin);

            prenom[strlen(prenom)-1] = '\0';

            printf("Ville du voeu : ");

            fgets(ville, 30, stdin);

            ville[strlen(ville)-1] = '\0';

            printf("Département du voeu : ");

            fgets(departement, 22, stdin);

            departement[strlen(departement)-1] = '\0';

            res = rechercheCandidatAdmis(liste, nom, prenom, &trouveCandidat, &trouveVoeu, ville, departement);

            if (trouveCandidat == 0){

                printf("Candidat non trouvé\n");

                break;

            }

            if (trouveVoeu == 0){

                printf("Voeu non trouvé\n");

                break;

            }

            clear();

            switch (res){

                case 1:

                    printf("%s %s a été admis(e) !\n", nom, prenom);

                    break;

                case 2:

                    printf("%s %s est en file d'attente !\n", nom, prenom);

                    break;

                case -1:

                    printf("%s %s a été refusé(e) !\n", nom, prenom);

                    break;

                default:

                    break;

            }

            break;

        case 9:

            clear();

            return;
        
        default:

            affichageErreurSaisie();

    }
}

/**
 * \brief Recherche un candidat dans la liste donnée
 * \param[in] liste Liste des candidats
 * \param[in] nom Nom du candidat
 * \param[in] prenom Prénom du candidat
 * \param[in, out] trouve Si le candidat est dans la liste ou non
 * \return int
 * 
 * Fonction qui recherche un candidat dans la liste entrée en paramètres. Elle retourne l'emplacement du candidat dans la liste.
 */

int rechercheCandidat(ListeCandidats liste, char* nom, char* prenom, int *trouve){

    int pos=0;

    while(liste != NULL) {

        if((strcmp(liste->c.nom, nom) == 0) && (strcmp(liste->c.prenom, prenom) == 0)){

            afficher1Candidat(liste->c);

            *trouve=1;

            return pos;

        }

        pos = pos+1;

        liste=liste->suiv;

    }

    *trouve=-1;

    return pos;

}

/**
 * \brief Gère les candidats en fonction de la note minimum et le maximum d'admis
 * \param[in] liste Liste des candidats
 * \param[in] tlog Taille de la liste des candidats
 * \param[in] noteMin Note minimum pour être accepté
 * \param[in] maxAdmis Maximum admis dans l'iut
 * \return void
 * 
 * Fonction qui gère les candidats en fonction de la note minimum et le maximum d'admis, puis affiche les résultats. Elle ne retourne rien.
 */

void gestionCandidats(ListeCandidats liste, int tlog, float noteMin, int maxAdmis){

    int nbValide = 0;

    ListeCandidats listeC = listeCandidatsNouv(), admis = listeCandidatsNouv();

    File attente = filenouv();

    while(noteMin <= 0){

        printf("La note minimum n'est pas définie ou est égal à 0.\n");

        printf("Note minimum : ");

        scanf("%f", &noteMin);

    }

    while(maxAdmis <= 0){

        printf("Le maximum doit être supérieur à 0.\n");

        printf("Maximum admis : ");

        scanf("%d", &maxAdmis);

        clear();

    }

    listeC = rechercheVoeux(liste);

    if(tlog == -1) return;

    printf("\033[1;37m"); // Gras

    printf("\tClassement\n------------------------------\n");

    printf("\033[0m"); // Reset couleur

    admis = changerDecisionAdmis(listeC, maxAdmis, noteMin, &attente);

    affichageCandidats(admis);

    afficherFile(attente);

    nbValide = longueurListeCandidats(admis);

    if(nbValide == tlog) printf("Tous les candidats sont au-dessus de la note minimum !\n");

    if(nbValide == 0) printf("Il n'y a aucun candidat au-dessus de la note minimum...\n");

    printf("Nombre de candidats : %d\n", nbValide);

}

/**
 * \brief Change la décision dans la liste de candidats
 * \param[in] liste Liste des candidats
 * \param[in] tlog Taille de la liste des candidats
 * \param[in] noteMin Note minimum pour être accepté
 * \param[in] maxAdmis Maximum admis dans l'iut
 * \param[in, out] admis Liste des candidats admis
 * \param[in, out] attente Liste des candidats en attente
 * \return ListeCandidats
 * 
 * Fonction qui change la décision dans la liste suite à la note minimum et le maximum d'admis. Elle retourne la liste de candidats.
 */

ListeCandidats changeDecision(ListeCandidats liste, int tlog, float noteMin, int maxAdmis, ListeCandidats *admis, File *attente){

    ListeCandidats listeC = listeCandidatsNouv(), aux;

    while(noteMin <= 0){

        printf("La note minimum n'est pas définie ou est égal à 0.\n");

        printf("Note minimum : ");

        scanf("%f", &noteMin);

    }

    while(maxAdmis <= 0){

        printf("Le maximum doit être supérieur à 0.\n");

        printf("Maximum admis : ");

        scanf("%d%*c", &maxAdmis);

        clear();

    }

    listeC = rechercheVoeux(liste);

    aux = copierListeCandidats(listeC);

    if(tlog == -1) exit(1);

    printf("\033[1;37m"); // Gras

    printf("\033[0m"); // Reset couleur

    liste = remettreAZeroListe(admis, liste);

    liste = remettreAZero(attente, liste);

    *admis = changerDecisionAdmis(aux, maxAdmis, noteMin, attente);

    liste = actualiseDecisionListe(*admis, liste, 1);

    liste = actualiseDecision(*attente, liste, 2);

    liste = refuser(listeC, liste, noteMin);

    return liste;

}

/**
 * \brief Efface la liste d'attente
 * \param[in, out] file File d'attente liée aux candidats
 * \param[in] ListeC Liste des candidats
 * \return ListeCandidats
 * 
 * Fonction qui efface la liste d'attente liée aux candidats. Elle retourne la nouvelle liste des candidats.
 */

ListeCandidats remettreAZero(File *file, ListeCandidats listeC){

    int longueurAdmis = longueurFile(*file);

    int i;

    for (i = 0 ; i < longueurAdmis ; i++){

        listeC = listerCandidats(*file, listeC, 0);

        (*file) -> c.choix = remetChoixZero((*file) -> c.choix);

        *file = (*file) -> suiv;

    }

    freeFile(*file);

    *file = filenouv();

    return listeC;

}

/**
 * \brief Efface la liste d'admis
 * \param[in, out] listeAdmis Liste des candidats admis
 * \param[in] ListeC Liste des candidats
 * \return ListeCandidats
 * 
 * Fonction qui efface la liste d'admis liée aux candidats. Elle retourne la nouvelle liste des candidats.
 */

ListeCandidats remettreAZeroListe(ListeCandidats *listeAdmis, ListeCandidats listeC){

    int longueurAdmis = longueurListeCandidats(*listeAdmis);

    int i;

    for (i = 0 ; i < longueurAdmis ; i++){

        listeC = listerListeCandidats(*listeAdmis, listeC, 0);

        (*listeAdmis) -> c.choix = remetChoixZero((*listeAdmis) -> c.choix);

        *listeAdmis = (*listeAdmis) -> suiv;

    }

    freeListe(*listeAdmis);

    *listeAdmis = listeCandidatsNouv();

    return listeC;

}

/**
 * \brief Efface la file en paramètres
 * \param[in] file File à vider
 * \return void
 * 
 * Fonction qui efface la file entrée en paramètres. Elle ne retourne rien.
 */

void freeFile(File file){

    MaillonFile *maillonSuivant;

    int longueur = longueurFile(file), i;

    for (i = 0 ; i < longueur ; i++){

        maillonSuivant = (file) -> suiv;

        free(file);

        file = maillonSuivant;

    }

}

/**
 * \brief Efface la liste des candidats en paramètres
 * \param[in] liste Liste des candidats à vider
 * \return void
 * 
 * Fonction qui efface la liste des candidats entrée en paramètres. Elle ne retourne rien.
 */

void freeListe(ListeCandidats liste){

    MaillonCandidat *maillonSuivant;

    int longueur = longueurListeCandidats(liste), i;

    for (i = 0 ; i < longueur ; i++){

        maillonSuivant = (liste) -> suiv;

        free(liste);

        liste = maillonSuivant;

    }

}

/**
 * \brief Remet à zéro la liste des choix en paramètres
 * \param[in] liste Liste des choix à vider
 * \return ListeChoix
 * 
 * Fonction qui remet à zéro la liste des choix entrée en paramètres. Elle retourne la nouvelle liste de choix.
 */

ListeChoix remetChoixZero(ListeChoix liste){

    ListeChoix aux;

    aux = liste;

    while (liste != NULL){

        liste = modifieDecision(liste, 0);

        liste = liste -> suiv;

    }

    return aux;

}

/**
 * \brief Copie la liste des candidats
 * \param[in] liste Liste des candidats à copier
 * \return ListeCandidats
 * 
 * Fonction qui copie la liste des candidats entrée en paramètres. Elle quitte le programme si il y a un problème malloc, sinon elle retourne la liste de candidats.
 */


ListeCandidats copierListeCandidats(ListeCandidats liste){
    
    if (liste == NULL) return NULL;

    MaillonCandidat *copie = malloc(sizeof(MaillonCandidat));

    if (copie == NULL) {

        printf("Erreur lors de l'allocation de mémoire pour la copie de la liste de candidats");

        exit(1);

    }

    copie -> c = liste -> c;

    copie -> suiv = copierListeCandidats(liste -> suiv);

    return copie;

}

/**
 * \brief Refuse le candidat en dessous de la note minimum
 * \param[in] listeC Liste des candidats
 * \param[in] liste Liste à retourner
 * \param[in] noteMin Note minimum
 * \return ListeCandidats
 * 
 * Fonction qui parcourt la liste de candidats pour refuser les candidats en dessous de la note minimum. Elle retourne la nouvelle liste de candidats.
 */


ListeCandidats refuser(ListeCandidats listeC, ListeCandidats liste, float noteMin){

    while (listeC != NULL){

        if (((listeC -> c.notes[0] * 2 + listeC -> c.notes[1] + listeC -> c.notes[2] * 2 + listeC -> c.notes[3] * 4) / 9) < noteMin)

            liste = listerRefuse(listeC, liste, -1);

        listeC = listeC -> suiv;

    }

    return liste;

}

/**
 * \brief Refuse les candidats en dessous de la note minimum dans la liste
 * \param[in] listeC Liste des candidats
 * \param[in] liste Liste à retourner
 * \param[in] decision Décision du département
 * \return ListeCandidats
 * 
 * Fonction qui parcourt la liste de candidats pour lister les candidats refusés. Elle retourne la liste de candidats.
 */

ListeCandidats listerRefuse(ListeCandidats listeC, ListeCandidats liste, int decision){

    ListeCandidats aux;

    aux = liste;

    while (liste != NULL){

        if (listeC -> c.numCandid == liste -> c.numCandid)

            liste -> c.choix = modifieDecision(liste -> c.choix, decision);

        liste = liste -> suiv;

    }

    return aux;

}

/**
 * \brief Actualise les décisions liées aux candidat
 * \param[in] file File des candidats en attente
 * \param[in] liste Liste des candidats
 * \param[in] decision Décision à insérer
 * \return ListeCandidats
 * 
 * Fonction qui actualise les décisions liées au candidat (Si le candidat refuse, on actualise la file d'attente). Elle retourne la nouvelle liste de candidats.
 */

ListeCandidats actualiseDecision(File file, ListeCandidats liste, int decision){

    int i;

    for (i = 0 ; i < longueurFile(file) ; i++){

        liste = listerCandidats(file, liste, decision);

        file = file -> suiv;

    }

    return liste;

}

/**
 * \brief Actualise les décisions liées aux candidats dans la liste
 * \param[in] listeAdmis Liste des candidats admis
 * \param[in] liste Liste des candidats
 * \param[in] decision Décision à insérer
 * \return ListeCandidats
 * 
 * Fonction qui actualise les décisions liées aux candidats (Si un candidat refuse, on actualise la file d'attente). Elle retourne la nouvelle liste de candidats.
 */

ListeCandidats actualiseDecisionListe(ListeCandidats listeAdmis, ListeCandidats liste, int decision){

    int i, longueur;

    longueur = longueurListeCandidats(listeAdmis);

    for (i = 0 ; i < longueur ; i++){

        liste = listerListeCandidats(listeAdmis, liste, decision);

        listeAdmis = listeAdmis -> suiv;

    }

    return liste;

}

/**
 * \brief Actualise les décisions des départements piur le candidat
 * \param[in] file File des candidats en attente
 * \param[in] liste Liste des candidats
 * \param[in] decision Décision à insérer
 * \return ListeCandidats
 * 
 * Fonction qui actualise les décisions des départements pour le candidat. Elle retourne la nouvelle liste de candidats.
 */

ListeCandidats listerCandidats(File file, ListeCandidats liste, int decision){

    ListeCandidats aux;

    aux = liste;

    while (liste != NULL){

        if (file -> c.numCandid == liste -> c.numCandid)

            liste -> c.choix = modifieDecision(liste -> c.choix, decision);

        liste = liste -> suiv;

    }

    return aux;

}  

/**
 * \brief Actualise les décisions des départements dans la liste
 * \param[in] listeAdmis Liste des candidats admis
 * \param[in] liste Liste des candidats
 * \param[in] decision Décision à insérer
 * \return ListeCandidats
 * 
 * Fonction qui actualise les décisions des départements dans la liste. Elle retourne la nouvelle liste de candidats.
 */

ListeCandidats listerListeCandidats(ListeCandidats listeAdmis, ListeCandidats liste, int decision){

    ListeCandidats aux;

    aux = liste;

    while (liste != NULL){

        if (listeAdmis -> c.numCandid == liste -> c.numCandid)

            liste -> c.choix = modifieDecision(liste -> c.choix, decision);

        liste = liste -> suiv;

    }

    return aux;

}

/**
 * \brief Modifie les décisions des départements dans la liste de choix
 * \param[in] liste Liste des choix
 * \param[in] decision Décision à modifier
 * \return ListeChoix
 * 
 * Fonction qui modifie les décisions des départements dans la liste. Elle retourne la nouvelle liste de choix.
 */

ListeChoix modifieDecision(ListeChoix liste, int decision){

    ListeChoix aux;

    aux = liste;

    int longueur = longueurListeChoix(liste), i;

    for (i = 0 ; i < longueur ; i++){

        if (strcmp(liste -> c.departement, "Informatique") == 0 && strcmp(liste -> c.ville, "Clermont-Ferrand") == 0)

            liste -> c.decision = decision;

        /*

        //TEST

        if (strcmp(liste -> c.departement, "Biologie") == 0 && strcmp(liste -> c.ville, "Clermont-Ferrand") == 0)

            liste -> c.decision = decision;

        //FIN TEST

        */

        liste = liste -> suiv;

    }

    return aux;

}

/**
 * \brief Change les candidats (admis ou non)
 * \param[in] listeC Liste des candidats
 * \param[in] maxAdmis Maximum admis
 * \param[in] noteMin Note minimum
 * \param[in, out] attente File d'attente
 * \return ListeCandidats
 * 
 * Fonction qui change les candidats (admis ou non) dans la liste des candidats. Elle retourne la nouvelle liste de candidats admis.
 */

ListeCandidats changerDecisionAdmis(ListeCandidats listeC, int maxAdmis, int noteMin, File* attente){

    int i, j, pge, longueur;

    Candidat cpge;

    ListeCandidats admis = listeCandidatsNouv();

    for (j = 0 ; j < maxAdmis ; j++){
        
        cpge = plusGrand(listeC, &pge);

        if (pge >= noteMin){

            admis = insererCandidats(admis, cpge);

            listeC = supprimerCandidats(listeC, cpge.numCandid);

        }

    }

    longueur = longueurListeCandidats(listeC);

    for (i = 0 ; i < longueur ; i++){
        
        cpge = plusGrand(listeC, &pge);

        if (pge >= noteMin){

            *attente = adjonctionEnQueueFile(*attente, cpge);

            listeC = supprimerCandidats(listeC, cpge.numCandid);

        }

    }

    return admis;

}

/**
 * \brief Cherche le candidat avec la meilleure moyenne
 * \param[in] liste Liste des candidats
 * \param[in, out] pge Plus grand élément
 * \return Candidat
 * 
 * Fonction qui cherche le candidat qui a la meilleure moyenne dans la liste. Elle retourne le candidat.
 */

Candidat plusGrand(ListeCandidats liste, int *pge){

    *pge = 0;

    Candidat cpge;

    while(liste != NULL){

        if (((liste -> c.notes[0] * 2 + liste -> c.notes[1] + liste -> c.notes[2] * 2 + liste -> c.notes[3] * 4) / 9) >= *pge){

            *pge = (liste -> c.notes[0] * 2 + liste -> c.notes[1] + liste -> c.notes[2] * 2 + liste -> c.notes[3] * 4) / 9;

            cpge = liste -> c;

        }

        liste = liste -> suiv;

    }
    
    return cpge;

}

/**
 * \brief Lit les moyennes de chaque candidats dans le fichier notesCandidats.don
 * \param[in, out] tabNumCandidat Tableau des numéros des candidats
 * \param[in, out] tabMoyennes Tableau des moyennes des candidats
 * \return int
 * 
 * Fonction qui lit les moyennes de chaque candidats dans le fichier notesCandidats.don. Elle retourne -1 si il y a un problème d'ouverture, sinon elle retourne le nombre de candidats.
 */

int lectureMoyennes(int tabNumCandidat[30], float tabMoyennes[30]){

    FILE* flot;

    int i=0;

    flot=fopen("notesCandidats.don", "r");

    if(flot == NULL) {

        printf("Problème ouverture fichier [notesCandidats.don]\n");

        return -1;

    }

    while(!feof(flot)){

        fscanf(flot, "%d\n%f\n", &tabNumCandidat[i], &tabMoyennes[i]);

        i++;

    }

    fclose(flot);

    return i;

}

/**
 * \brief Affiche l'erreur de saisie
 * \param[in] void
 * \return void
 * 
 * Fonction qui affiche une erreur si l'utilisateur a rentré une saisie invalide. Elle ne retourne rien.
 */

void affichageErreurSaisie(void){

    clear();

    printf("-------------------------------\n");

    printf("\033[1;31m"); // Rouge

    printf("Erreur : Saisie incorrecte.\n\a"); // \a -> Cloche

    printf("\033[0m"); // Reset couleur

    printf("-------------------------------\n");

}

/**
 * \brief Affiche le menu administrateur
 * \param[in] void
 * \return void
 * 
 * Fonction qui affiche le menu administrateur. Elle ne retourne rien.
 */

void affichageMenuAdministrateur(void){

    printf("-------------------------------\n");
            
    printf("\033[1;37m"); // Gras

    printf("\tMENU ADMINISTRATEUR\t\n");

    printf("\033[0m"); // Reset couleur
        
    printf("-------------------------------\n\n");

    printf("(1) Modifier le nombre de places dans un département\n");
    
    printf("(2) Créer un département dans un IUT\n");

    printf("(3) Supprimer un département d'un IUT\n");

    printf("(4) Lancer et arrêter la phase de candidature\n");
    
    printf("(5) Modifier le nom du responsable d'un département\n");

    printf("(6) Afficher toutes les informations\n");

    printf("(7) Afficher un candidat en particulier\n");

    printf("(8) Afficher les candidats présents dans un département\n");

    printf("\n(9) Retour\n");   

}

/**
 * \brief Affiche mot de passe incorrect
 * \param[in] void
 * \return void
 * 
 * Fonction qui affiche une erreur si l'utilisateur a rentré un mot de passe incorrect. Elle ne retourne rien.
 */

void affichageErreurMdp(void){

    clear();

    printf("---------------------------------------------------\n");

    printf("\033[1;31m"); // Rouge

    printf("Mot de passe incorrect. ");

    printf("\033[0m"); // Reset couleur

    printf("Retour au menu principal.\n\a"); // \a -> Cloche

    printf("---------------------------------------------------\n");

}

/**
 * \brief Menu principal
 * \param[in] void
 * \return void
 * 
 * Fonction principale du programme, c'est le menu principal. Elle ne retourne rien.
 */

void menuPrincipal(void){

    int choix, tlog, etatPhase, tlog2, tlogAdmis;

    VilleIUT *tiut[20];

    ListeCandidats liste, admis = listeCandidatsNouv();

    File attente = filenouv();

    tlog = chargerFichier(tiut, 200);

    if (tlog == -2 || tlog == -1) return;

    admis = chargerBinListe(admis, "admis.bin", &tlogAdmis);

    attente = chargerBinFile(attente, "attente.bin");

    etatPhase = phaseCandid();
    
    liste = listeCandidatsNouv();

    /* PARTIE RESERVEE AU DEBUGGAGE

    //liste = chargerTxtCandidats(liste, &tlog2);

    //sauverCandidats(liste, tlog2);*/

    liste = listeCandidatsNouv();

    liste = chargerBinCandidats(liste, &tlog2);

    attributionNote(liste, tlog2);

    while(choix != 9){

        if (etatPhase == 0)

            affichageMenuPrincipal();

        else

            affichageMenuPrincipal2();

        choix = fonctionChoix();

        switchPrincipal(choix, tiut, tlog, &etatPhase, liste, &admis, &attente);

    }

    sauverFile(attente, "attente.bin");

    printf("Fichier [attente.bin] sauvegardé !\n");

    sauverCandidats(admis, tlogAdmis, "admis.bin");

    printf("Fichier [admis.bin] sauvegardé !\n");

    sauvegarder(tiut, tlog);

    printf("Ficher [iut.don] sauvegardé !\n");

    sauverCandidats(liste, tlog2, "candidats.bin");

    printf("Ficher [candidats.bin] sauvegardé !\n");

    printf("Programme réalisé par Corentin LEMAIRE et Erwan MENAGER.\n");

}

/**
 * \brief Menu administrateur
 * \param[in] tiut Tableau des iut
 * \param[in] tlog Taille de tiut
 * \param[in, out] etatPhase État actuel de la phase
 * \param[in] liste Liste des candidats
 * \return void
 * 
 * Fonction qui gère le menu administrateur. Elle ne retourne rien.
 */

void sousMenuAdministrateur(VilleIUT **tiut, int tlog, int *etatPhase, ListeCandidats liste){

    int choix;

    char motDePasseAdmin[20];

    printf("Mot de passe administrateur : ");

    scanf("%*c");

    fgets(motDePasseAdmin, 20, stdin);

    motDePasseAdmin[strlen(motDePasseAdmin)-1] = '\0';

    if(strcmp("M.Hasbani>", motDePasseAdmin) != 0){

        affichageErreurMdp();

        return;

    }

    clear(); // Clear terminal

    while(choix != 9){

        affichageMenuAdministrateur();

        choix = fonctionChoix();

        switchAdministrateur(choix, tiut, tlog, etatPhase, liste);    
    
    }

}

/**
 * \brief Choix de l'utilisateur
 * \param[in] void
 * \return int
 * 
 * Fonction qui gère la saisie de l'utilisateur. Elle retourne la saisie de ce dernier.
 */

int fonctionChoix(void){

    int choix;

    printf("\nVotre choix : ");

    scanf("%d", &choix);

    return choix;

}

/**
 * \brief Permet de saisir une ville
 * \param[in] ville Ville saisie
 * \param[in] tiut Tableau des iut
 * \param[in] tlog Taille de tiut
 * \return int
 * 
 * Fonction qui permet de saisir une ville et de vérifier si elle existe. Elle retourne -1 si le nom n'existe pas, 0 sinon.
 */

int saisieVille(char ville[], VilleIUT** tiut, int tlog){

    int trouve;

    printf("Nom de la ville : ");

    scanf("%*c");

    fgets(ville, 22, stdin);

    if (strcmp(ville,"-1") == 0 ){

        return -1;

    }

    rechercheVille(tiut, ville, tlog, &trouve);

    while (strcmp(ville, " \n") == 0 || strcmp(ville, "\n") == 0){

        printf("Nom incorrect, réessayez !\n");

        printf("Quel département voulez-vous modifier ? ");

        fgets(ville, 22, stdin);

        if (strcmp(ville,"-1") == 0 ){

            return -1;

        }

    }

    ville[strlen(ville) - 1] = '\0';

    return 0;

}

/**
 * \brief Permet de saisir un département
 * \param[in] departement Département saisie
 * \return int
 * 
 * Fonction qui permet de saisir un département et de vérifier si il existe. Elle retourne -1 si le nom n'existe pas, 0 sinon.
 */

int saisieDept(char departement[]){

    printf("Quel département voulez-vous modifier ? ");

    fgets(departement, 22, stdin);

    if (strcmp(departement,"-1") == 0 ){

        return -1;

    }

    while (strcmp(departement, " \n") == 0 || strcmp(departement, "\n") == 0){

        printf("Nom incorrect, réessayez !\n");

        printf("Quel département voulez-vous modifier ? ");

        fgets(departement, 22, stdin);

        if (strcmp(departement,"-1") == 0 ){

            return -1;

        }

    }

    departement[strlen(departement) - 1] = '\0';

    return 0;

}

/**
 * \brief Permet de saisir un nombre de places
 * \param[in] void
 * \return int
 * 
 * Fonction qui permet de saisir un nombre de places. Elle retourne -1 si l'utilisateur entre -1, sinon elle retourne le nombre de places.
 */

int saisieNbPlace(void){

    int nbP;

    printf("Nombre de places : ");

    scanf("%d", &nbP);

    if (nbP == -1 ){

        return -1;

    }

    while(nbP < 0){

        printf("Nombre de places incorrect, réessayez !\n");

        printf("Nombre de places : ");

        scanf("%d", &nbP);

        if (nbP == -1 ){

            return -1;

        }

    }

    return nbP;

}

/**
 * \brief Permet de saisir un nouveau responsable
 * \param[in] resp Responsable saisie
 * \return int
 * 
 * Fonction qui permet de saisir un nouveau responsable. Elle retourne -1 si l'utilisateur entre -1, 0 sinon.
 */

int saisieNvResp(char resp[]){

    printf("Nom du nouveau responsable : ");

    fgets(resp, 22, stdin);

    if (strcmp(resp,"-1") == 0 ){

        return -1;

    }

    while (strcmp(resp, " \n") == 0 || strcmp(resp, "\n") == 0){

        printf("Nom du responsable incorrect, réessayez !\n");

        printf("Nom du nouveau responsable : ");

        fgets(resp, 22, stdin);

        if (strcmp(resp,"-1") == 0 ){

            return -1;

        }

    }

    resp[strlen(resp) - 1] = '\0';

    return 0;

}

/**
 * \brief Affiche menu utilisateur
 * \param[in] void
 * \return void
 * 
 * Fonction qui affiche le menu utilisateur. Elle ne retourne rien.
 */

void affichageSousMenuUtilisateur(void){

    printf("-------------------------------\n");
            
    printf("\033[1;37m"); // Gras

    printf("\tMENU UTILISATEUR\t\n");

    printf("\033[0m"); // Reset couleur
    
    printf("-------------------------------\n\n");

    printf("(1) Voir les villes où il y a un IUT\n");

    printf("(2) Voir les départements d'un IUT\n");

    printf("(3) Voir le nombre de places en première année d'un département d'un IUT\n");

    printf("(4) Voir les IUT possédant un département en particulier\n");

    printf("(5) Mes candidatures\n");

    printf("\n(9) Retour\n"); 

}

/**
 * \brief Affiche menu responsable
 * \param[in] void
 * \return void
 * 
 * Fonction qui affiche le menu responsable. Elle ne retourne rien.
 */

void affichageSousMenuResponsable(void){

    printf("-------------------------------\n");
            
    printf("\033[1;37m"); // Gras

    printf("\tMENU RESPONSABLE\t\n");

    printf("\033[0m"); // Reset couleur

    printf("Clermont-Ferrand | Informatique\n");
    
    printf("-------------------------------\n\n");

    printf("(1) Afficher tous les candidats\n");

    printf("(2) Afficher tous les candidats triés par ordre alphabétique\n");

    printf("(3) Afficher les moyennes des candidats\n");

    printf("(4) Afficher les candidats au-dessus de la note minimum\n");

    printf("(5) Lancer les admissions\n");

    printf("(6) Consulter les files d'admis et d'attente\n");

    printf("(7) Afficher un candidat en particulier\n");

    printf("(8) Voir la décision d'un candidat pour un voeu\n");

    printf("\n(9) Retour\n"); 

}

/**
 * \brief Affiche les villes
 * \param[in] tiut Tableau des iut
 * \param[in] tlog Taille de tiut
 * \return void
 * 
 * Fonction qui affiche les villes présentes dans le tableau tiut. Elle ne retourne rien.
 */

void afficherVillesIUT(VilleIUT **tiut, int tlog){

    int pos;

    clear();

    printf("-------------------------------\n");

    for (pos = 0 ; pos < tlog ; pos++)

        printf("N°%d : %s\n", pos + 1, tiut[pos]->ville);

}

/**
 * \brief Affiche les départements de la ville
 * \param[in] tiut Tableau des iut
 * \param[in] pos Position de la ville
 * \return void
 * 
 * Fonction qui affiche les départements de la ville du tableau tiut. Elle ne retourne rien.
 */

void afficherDepartementsIUT(VilleIUT **tiut, int pos){

    clear();

    printf("-------------------------------\n");

    afficherListe(tiut[pos] -> ldept);

}

/**
 * \brief Switch pour le menu utilisateur
 * \param[in] choix Choix de l'utilisateur
 * \param[in] tiut Tableau des iut
 * \param[in] tlog Taille de tiut
 * \param[in] liste Liste des candidats
 * \param[in, out] admis Liste des candidats admis
 * \param[in, out] attente Liste des candidats en attente
 * \param[in, out] etatPhase État actuel de la phase
 * \return void
 * 
 * Fonction qui fait le switch pour le menu utilisateur. Elle ne retourne rien.
 */

void switchUtilisateur(int choix, VilleIUT **tiut, int tlog, ListeCandidats liste, ListeCandidats *admis, File *attente, int *etatPhase){

    int pos, trouve = 0, i;

    char ville[30], departement[30], nom[30], prenom[30];

    switch (choix){

        case 1:

            afficherVillesIUT(tiut, tlog);

            break;

        case 2:

            printf("Ville de l'IUT : ");

            scanf("%s", ville);

            pos = rechercheVille(tiut, ville, tlog, &trouve);

            if (trouve == 0){

                clear();

                printf("-------------------------------\n");

                printf("\033[1;31m"); // Rouge

                printf("Erreur : Ville inexistante !\n");

                printf("\033[0m"); // Reset couleur

                return;

            }

            afficherDepartementsIUT(tiut, pos);

            break;

        case 3:

            nbPDepartement(tiut, tlog);

            break;

        case 4:

            printf("Département recherché : ");

            scanf("%s", departement);

            clear();

            printf("\t%s\n", departement);

            printf("-------------------------------\n");

            rechercheTousDepartement(tiut, tlog, departement, &trouve);

            if (trouve != 1){

                printf("-------------------------------\n");

                printf("\033[1;31m"); // Rouge

                printf("Erreur : Département inexistant !\n");

                printf("\033[0m"); // Reset couleur

            }

            break;

        case 5:

            clear();

            scanf("%*c");

            printf("Nom du candidat : ");

            fgets(nom, 30, stdin);

            nom[strlen(nom)-1] = '\0';

            printf("Prénom du candidat : ");

            fgets(prenom, 30, stdin);

            clear();

            printf("\tVotre profil\n");

            printf("-------------------------------\n");

            prenom[strlen(prenom)-1] = '\0';

            pos = rechercheCandidat(liste, nom, prenom, &trouve);

            if(trouve == -1) {

                printf("\033[1;31m"); // Rouge

                printf("Erreur : Candidat non trouvé !\n");

                printf("\033[0m"); // Reset couleur

                return;

            }

            for (i = 0 ; i < pos ; i ++) liste = liste -> suiv;

            sousMenuCandidature(liste -> c, tiut, tlog, admis, attente, etatPhase);

            break;

        case 9:

            clear(); // Clear terminal

            return;
        
        default:

            clear(); // Clear terminal

            affichageErreurSaisie();

    }
}

/**
 * \brief Menu candidature
 * \param[in] c Candidat
 * \param[in] tiut Tableau des iut
 * \param[in] tlog Taille de tiut
 * \param[in, out] admis Liste des candidats admis
 * \param[in, out] attente Liste des candidats en attente
 * \param[in, out] etatPhase État actuel de la phase
 * \return void
 * 
 * Fonction qui gère le menu candidature. Elle ne retourne rien.
 */

void sousMenuCandidature(Candidat c, VilleIUT** tiut, int tlog, ListeCandidats *admis, File *attente, int *etatPhase){

    int choix = 0;

    while (choix != 9){

        affichageSousMenuCandidature();

        choix = fonctionChoix();

        switchCandidature(choix, c, tiut, tlog, admis, attente, etatPhase);

    }

}

/**
 * \brief Affiche le menu candidature
 * \param[in] void
 * \return void
 * 
 * Fonction qui affiche le menu candidature. Elle ne retourne rien.
 */

void affichageSousMenuCandidature(void){

    printf("-------------------------------\n");
            
    printf("\033[1;37m"); // Gras

    printf("\tMENU CANDIDATURE\t\n");

    printf("\033[0m"); // Reset couleur
    
    printf("-------------------------------\n\n");

    printf("(1) Mon profil\n");

    printf("(2) Ajouter une candidature\n");

    printf("(3) Supprimer une candidature\n");

    printf("(4) Voir les décisions\n");

    printf("(5) Valider un voeu\n");

    printf("\n(9) Retour\n"); 

}

/**
 * \brief Switch pour le menu candidature
 * \param[in] choix Choix de l'utilisateur
 * \param[in] tiut Tableau des iut
 * \param[in] tlog Taille de tiut
 * \param[in, out] admis Liste des candidats admis
 * \param[in, out] attente Liste des candidats en attente
 * \param[in, out] etatPhase État actuel de la phase
 * \return void
 * 
 * Fonction qui fait le switch pour le menu candidature. Elle ne retourne rien.
 */

void switchCandidature(int choix, Candidat c, VilleIUT** tiut, int tlog, ListeCandidats *admis, File *attente, int *etatPhase) {

    int trouve, trouve2, trouve3, decision, validation;

    char ville[30], departement[30], nvVille[30];

    Choix nvChoix;

    switch(choix){

        case 1:

            clear();

            printf("\tVotre profil \n");

            printf("-------------------------------\n");

            afficher1Candidat(c);

            break;

        case 2:

            clear();

            if(*etatPhase == 0){

                printf("-------------------------------\n");

                printf("\033[1;31m"); // Rouge

                printf("Erreur : Phase de candidature inactive !\n");

                printf("\033[0m"); // Reset couleur

                break;

            }

            scanf("%*c");

            printf("Ville : ");

            fgets(ville, 30, stdin);

            ville[strlen(ville)-1] = '\0';

            strcpy(nvChoix.ville,ville);

            printf("Département : ");

            fgets(departement, 30, stdin);

            departement[strlen(departement)-1] = '\0';

            strcpy(nvChoix.departement,departement);

            nvChoix.decision=0;

            nvChoix.validation=0;

            trouve=rechercheVille(tiut, ville, tlog, &trouve2);

            if(trouve != 1) {

                printf("-------------------------------\n");

                printf("\033[1;31m"); // Rouge

                printf("Erreur : Département ou ville inexistant(e) !\n");

                printf("\033[0m"); // Reset couleur

                return;

            }

            rechercheTousDepartement(tiut, tlog, nvVille, &trouve3);

            c.choix=ajouterChoix(c.choix, nvChoix);

            printf("-------------------------------\n");

            printf("Voeux ajouté !\n");

            break;

        case 3:

            clear();

            if(*etatPhase == 0){

                printf("-------------------------------\n");

                printf("\033[1;31m"); // Rouge

                printf("Erreur : Phase de candidature inactive !\n");

                printf("\033[0m"); // Reset couleur

                break;

            }

            scanf("%*c");

            printf("Ville : ");

            fgets(ville, 30, stdin);

            ville[strlen(ville)-1] = '\0';

            strcpy(nvChoix.ville, ville);

            printf("Département : ");

            fgets(departement, 30, stdin);

            departement[strlen(departement)-1] = '\0';

            ListeChoix save = c.choix;

            c.choix = supprimerVoeuxCandidat(c.choix, ville, departement, &trouve);

            //printf("%s\n", trouve);

            if(trouve == 2){

                printf("-------------------------------\n");

                printf("Candidature effacée !\n");

            }

            else {

                printf("-------------------------------\n");

                printf("\033[1;31m"); // Rouge

                printf("Erreur : Candidature inexistante !\n");

                printf("\033[0m"); // Reset couleur

            }

            c.choix = save;

            break;

        case 4:

            clear();

            printf("------------Admis-------------\n\n");

            affichageCandidatAdmis(c, 1);

            printf("----------En attente-----------\n\n");

            affichageCandidatAdmis(c, 2);

            printf("------------Refusé-------------\n\n");

            affichageCandidatAdmis(c, -1);

            break; 

        case 5:

            clear();
            
            scanf("%*c");

            printf("Ville : ");

            fgets(ville, 30, stdin);

            ville[strlen(ville)-1] = '\0';

            printf("Département : ");

            fgets(departement, 30, stdin);

            departement[strlen(departement)-1] = '\0';

            trouve = rechercheCandidatVoeu(c.choix, ville, departement, &validation, &decision);

            if (trouve == 0){

                printf("Voeu non présent !\n");

                break;

            }

            if (decision != 1){

                printf("Vous n'êtes pas admis dans cette formation !\n");

                break;

            }

            if (validation != 0){

                printf("Vous avez déjà validé vos voeux !\n");

                break;

            }

            c.choix = validerVoeux(c.choix, ville, departement, admis, attente, c.numCandid);

            break;

        case 9:

            clear();

            return;

        default:

            clear();

            affichageErreurSaisie();

    }

}

/**
 * \brief Recherche un voeux précis
 * \param[in] liste Liste des candidats
 * \param[in] ville Nom de la ville
 * \param[in] departement Nom du département
 * \param[in, out] validation Validation du candidat
 * \param[in, out] decision Decision du département
 * \return int
 * 
 * Fonction qui recherche un voeux entré en paramètres. Elle retourne 0 si elle n'a rien trouvé, 1 sinon.
 */

int rechercheCandidatVoeu(ListeChoix liste, char* ville, char* departement, int *validation, int *decision){

    if (liste ==  NULL) return 0;

    if((strcmp(liste -> c.ville, ville) == 0) && (strcmp(liste -> c.departement, departement) == 0)){

        *decision = liste -> c.decision;

        *validation = liste -> c.validation;

        return 1;

    }

    return rechercheCandidatVoeu(liste -> suiv, ville, departement, validation, decision);

}

/**
 * \brief Affiche les candidats admis
 * \param[in] c Candidat
 * \param[in] decision Décision du département
 * \return void
 * 
 * Fonction qui affiche les candidats admis. Elle ne retourne rien.
 */

void affichageCandidatAdmis(Candidat c, int decision){

    ListeChoix save = listeChoixNouv();

    save = c.choix;

    while (c.choix !=NULL){

        if (c.choix -> c.decision == decision)

            printf("%s\t%s\n\n", c.choix -> c.ville, c.choix -> c.departement);

        c.choix = c.choix -> suiv;

    }

    c.choix = save;

}

/**
 * \brief Supprime un voeux d'un candidat
 * \param[in] choix Liste des choix du candidat
 * \param[in] ville Ville du voeux du candidat
 * \param[in] departement Département du voeux du candidat
 * \param[in, out] trouve Liste des candidats
 * \return ListeChoix
 * 
 * Fonction qui supprime un voeux d'un candidat entré en paramètres. Elle retourne la liste des choix de ce dernier.
 */

ListeChoix supprimerVoeuxCandidat(ListeChoix choix, char* ville, char* departement, int *trouve){

    if(choix == NULL) return choix;

    if((strcmp(choix->c.ville, ville) == 0) && (strcmp(choix->c.departement, departement) == 0)) {

        *trouve = 2;

        return supprimerVoeuxCandidatEnTete(choix);

    }

    choix -> suiv = supprimerVoeuxCandidat(choix->suiv, ville, departement, trouve);

    return choix;

}

/**
 * \brief Supprime un voeux en tête
 * \param[in] choix Liste des choix du candidat
 * \return ListeChoix
 * 
 * Fonction qui supprime un voeux en tête d'un candidat entré en paramètres. Elle retourne la liste des choix de ce dernier.
 */

ListeChoix supprimerVoeuxCandidatEnTete(ListeChoix choix){

    MaillonChoix *aux;

    if(choix == NULL){

        printf("Opération interdite\n");

        exit(1);

    }

    aux = choix;

    choix=choix->suiv;

    free(aux);

    return choix;
}

/**
 * \brief Recherche un département dans tous les iut
 * \param[in] tiut Tableau des iut
 * \param[in] tlog Taille de tiut
 * \param[in] departement Département
 * \param[in, out] trouve2 Si le département est trouvé ou non
 * \return void
 * 
 * Fonction qui recherche un département dans tous les iut existants. Elle ne retourne rien.
 */

void rechercheTousDepartement(VilleIUT** tiut, int tlog, char* departement, int *trouve2){

    int i, trouve;

    for (i = 0 ; i < tlog - 1 ; i ++){

        rechercheDepartement(tiut, tiut[i] -> ville, departement, tlog, &trouve);

        if (trouve == 1){

            printf("%s\n", tiut[i] -> ville);

            *trouve2 = 1;

        }

    }

}

/**
 * \brief Affiche le nombre de places d'un département
 * \param[in] tiut Tableau des iut
 * \param[in] tlog Taille de tiut
 * \return void
 * 
 * Fonction qui affiche le nombre de places d'un département précis. Elle ne retourne rien.
 */

void nbPDepartement(VilleIUT** tiut, int tlog){

    int pos, trouve;

    char ville[30], departement[30];

    printf("Ville de l'IUT : ");

    scanf("%s", ville);

    pos=rechercheVille(tiut, ville, tlog, &trouve);

    if (trouve == 0){

        clear();

        printf("-------------------------------\n");

        printf("\033[1;31m"); // Rouge

        printf("Erreur : Ville inexistante !\n");

        printf("\033[0m"); // Reset couleur

        return;

    }

    printf("Département de l'IUT de %s : ", ville);

    scanf("%s", departement);

    rechercheDepartement(tiut, ville, departement, tlog, &trouve);

    clear();

    printf("-------------------------------\n");

    if (trouve == 0){

        printf("\033[1;31m"); // Rouge

        printf("Erreur : Département inexistant !\n");

        printf("\033[0m"); // Reset couleur

        return;

    }

    printf("Nombre de places : %d\n", tiut[pos]->ldept->nbP);

}

/**
 * \brief Menu utilisateur
 * \param[in] tiut Tableau des iut
 * \param[in] tlog Taille de tiut
 * \param[in] liste Liste des candidats
 * \param[in, out] admis Liste des candidat admis
 * \param[in, out] attente File d'attente
 * \param[in, out] etatPhase État actuel de la phase
 * \return void
 * 
 * Fonction qui gère le menu utilisateur. Elle ne retourne rien.
 */

void sousMenuUtilisateur(VilleIUT **tiut, int tlog, ListeCandidats liste, ListeCandidats *admis, File *attente, int *etatPhase){

    int choix = 0;

    while (choix != 9){

        affichageSousMenuUtilisateur();

        choix = fonctionChoix();

        switchUtilisateur(choix, tiut, tlog, liste, admis, attente, etatPhase);

    }

}

/**
 * \brief Insère un département dans la liste
 * \param[in] l Liste des départements
 * \param[in] departement Département
 * \param[in] nbP Nombre de places
 * \param[in] nomResp Nom du responsable
 * \return ListeDept
 * 
 * Fonction qui insère un département. Elle retourne la nouvelle liste.
 */

ListeDept inserer(ListeDept l, char* departement, int nbP, char* nomResp){

    if (l == NULL)

		return insererEnTete(l, departement, nbP, nomResp);

	if (strcmp(departement, l -> departement) < 0)

		return insererEnTete(l, departement, nbP, nomResp);

	if (strcmp(departement, l -> departement) == 0)

		return l;

	l -> suiv = inserer(l->suiv, departement, nbP, nomResp);

	return l;

}

/**
 * \brief Insère un département en tête de liste
 * \param[in] l Liste des départements
 * \param[in] departement Département
 * \param[in] nbP Nombre de places
 * \param[in] nomResp Nom du responsable
 * \return ListeDept
 * 
 * Fonction qui insère un département en tête de liste. Elle retourne la nouvelle liste.
 */

ListeDept insererEnTete(ListeDept l, char* departement, int nbP, char* nomResp){

	MaillonDept *m;

	m = (MaillonDept *)malloc(sizeof(MaillonDept));

	if (m == NULL)

	{

		printf("Problème malloc\n");

		exit(1);

	}

	strcpy(m -> departement, departement);

    m -> nbP = nbP;

    strcpy(m -> nomResp, nomResp);

	m -> suiv = l;

	return m;

}

/**
 * \brief Supprime un département dans la liste
 * \param[in] l Liste des départements
 * \param[in] departement Département
 * \return ListeDept
 * 
 * Fonction qui supprime un département. Elle retourne la nouvelle liste.
 */

ListeDept supprimer(ListeDept l, char* departement){

    if (l == NULL)

        return l;

    if (strcmp(departement, l -> departement) < 0)

        return l;

    if (strcmp(departement, l -> departement) == 0)

        return supprimerEnTete(l);

    l -> suiv = supprimer(l->suiv, departement);

    return l;

}

/**
 * \brief Supprime un département en tête de liste
 * \param[in] l Liste des départements
 * \return ListeDept
 * 
 * Fonction qui supprime un département en tête de liste. Elle retourne la nouvelle liste.
 */

ListeDept supprimerEnTete(ListeDept l){

    MaillonDept *aux;

    if (l == NULL){

        printf("Opération interdite !\n");

        exit(1);

    }

    aux = l;

    l = l -> suiv;

    free(aux);

    return l;

}

/**
 * \brief Modifie le nombre de places d'un département
 * \param[in] tiut Tableau des iut
 * \param[in] positionTab Position du département dans le tableau
 * \param[in] position Position du département
 * \param[in] nbP Département
 * \return void
 * 
 * Fonction qui modifie le nombre de places d'un département. Elle ne retourne rien.
 */

void modifNbPlaces(VilleIUT* tiut[], int positionTab, int position, int nbP){

    int i;

    for (i = 0 ; i < position ; i ++){

        tiut[positionTab] -> ldept = tiut[positionTab] -> ldept -> suiv;

    }

    tiut[positionTab] -> ldept -> nbP = nbP;

}

/**
 * \brief Modifie le nom du responsable d'un département
 * \param[in] tiut Tableau des iut
 * \param[in] positionTab Position du département dans le tableau
 * \param[in] position Position du département
 * \param[in] nomResp Nom du responsable
 * \return void
 * 
 * Fonction qui modifie le nom du responsable d'un département. Elle ne retourne rien.
 */

void modifResp(VilleIUT* tiut[], int positionTab, int position, char* nomResp){

    int i;

    for (i = 0 ; i < position ; i ++){

        tiut[positionTab] -> ldept = tiut[positionTab] -> ldept -> suiv;

    }

    strcpy(tiut[positionTab] -> ldept -> nomResp, nomResp);

}

/**
 * \brief Efface l'affichage actuel
 * \param[in] void
 * \return void
 * 
 * Fonction qui efface l'affichage actuel. Elle ne retourne rien.
 */

void clear(void){

    printf("\e[1;1H\e[2J");

}

/**
 * \brief Initialise une nouvelle file
 * \param[in] void
 * \return File
 * 
 * Fonction qui initialise une nouvelle file. Elle retourne donc cette dernière.
 */


File filenouv(void){

    File f;

    f = NULL;

    return f;

}

/**
 * \brief Ajoute un candidat en file
 * \param[in] f File
 * \param[in] c Candidat à ajouter
 * \return File
 * 
 * Fonction qui ajoute un candidat en file. Elle retourne donc la nouvelle file.
 */

File adjonctionEnQueueFile(File f, Candidat c){

    MaillonFile *m;

    m = (MaillonFile *) malloc (sizeof(MaillonFile));

    if (m == NULL){

        printf("Pb malloc\n");

        exit(1);

    }

    m -> c = c;

    if (videFile(f)){

        f = m;

        m -> suiv = m;
        
        return f;

    }

    m -> suiv = f -> suiv;

    f -> suiv = m;

    f = m;

    return m;

}

/**
 * \brief Supprime un candidat en file
 * \param[in] f File
 * \return File
 * 
 * Fonction qui ajoute un candidat en file. Elle retourne donc la nouvelle file.
 */


File suppressionEnTeteFile(File f){

    MaillonFile *aux;

    if (videFile(f)){

        printf("Opération interdite\n");

        exit(1);

    }

    if (f == f -> suiv){

        free(f);

        return filenouv();

    }

    aux = f -> suiv;

    f -> suiv = aux -> suiv;

    free(aux);

    return f;

}

/**
 * \brief Affiche le candidat en tête de file
 * \param[in] f File
 * \return Candidat
 * 
 * Fonction qui affiche le candidat en tête de file. Elle retourne un candidat.
 */


Candidat teteFile(File f){

    if (videFile(f)) {

        printf("Opération interdite !\n");

        exit(1);

    }

    return f -> suiv -> c;

}

/**
 * \brief Calcule la longueur d'une file
 * \param[in] f File
 * \return int
 * 
 * Fonction qui calcule la longueur d'une file. Elle retourne donc la longueur de cette dernière.
 */

int longueurFile(File f){

    MaillonFile *c;
    
    int compteur = 0;

    if (videFile(f))

        return 0;

    c = f;

    while(c != f -> suiv){

        compteur = compteur + 1;

        f = f -> suiv;

    }

    return compteur + 1;

}

/**
 * \brief Vérifie si une file est vide
 * \param[in] f File
 * \return bool
 * 
 * Fonction qui vérifie si une file est vide. Elle retourne donc true ou false.
 */

bool videFile(File f){

    if (f == NULL) return true;

    return false;   

}

/**
 * \brief Affiche une file
 * \param[in] f File
 * \return void
 * 
 * Fonction qui affiche une file. Elle ne retourne rien.
 */

void afficherFile(File f){

    int i;

    for (i = 0; i < longueurFile(f); i++){

        afficher1Candidat(f -> suiv -> c);

        f = f -> suiv;

    }


}

ListeCandidats rechercheVoeux(ListeCandidats l){

    ListeCandidats listeC = listeCandidatsNouv();

    int trouve;

    while (l != NULL) {

        trouve = rechercheDeptCandidat(l -> c.choix);

        if (trouve == 1)

            listeC = insertionCandidat(listeC, l -> c);

        l = l -> suiv;

    }

    return listeC;

}

void rechercheDeptCandidatE(ListeChoix lChoix, char* nomVille, char* nomDep, ListeCandidats liste){

    while (lChoix != NULL){

        if ((strcmp(lChoix -> c.ville, nomVille) == 0) && (strcmp(lChoix -> c.departement, nomDep) == 0)) {

            afficher1Candidat(liste->c);

            printf("-------------------------------\n");

        }

        lChoix = lChoix -> suiv;

    }

}

int rechercheDeptCandidat(ListeChoix l){

    while (l != NULL){

        if ((strcmp(l -> c.ville, "Clermont-Ferrand\0") == 0) && (strcmp(l -> c.departement, "Informatique\0") == 0))

            return 1;

        l = l -> suiv;

    }

    return 0;

}

/**
 * \brief Tri selection
 * \param[in, out] tabNumCandidat Tableau des numéros des candidats
 * \param[in, out] tabMoyennes Tableau des moyennes des candidats
 * \param[in] tlog Taille de tabNumCandidat et tabMoyennes
 * \return void
 * 
 * Fonction qui trie les deux tableaux en paramètres. Elle ne retourne rien.
 */

void triSelection(int tabNumCandidat[30], float tabMoyennes[30], int tlog){

    int i,j,c;

    float c2;

    for(i=0;i<tlog-1;i++)

        for(j=i+1;j<tlog;j++)

            if ( tabMoyennes[i] < tabMoyennes[j] ) {

                c = tabNumCandidat[i];

                c2 = tabMoyennes[i];

                tabNumCandidat[i] = tabNumCandidat[j];

                tabMoyennes[i] = tabMoyennes[j];

                tabNumCandidat[j] = c;

                tabMoyennes[j] = c2;

            }

}

/**
 * \brief Sauvegarde la file dans un fichier
 * \param[in] file File à sauvegarder
 * \param[in] nomFichier Nom du fichier
 * \return void
 * 
 * Fonction qui sauvegarde la file dans un fichier entré en paramètres. Elle quitte le programme si il y a un problème d'ouverture, sinon elle ne retourne rien.
 */

void sauverFile(File file, char* nomFichier){

    FILE* flot;

    int longueur = longueurFile(file), i, j, longueur2;

    flot = fopen(nomFichier, "wb");

    if (flot == NULL){

        printf("Pb ouverture fichier : %s\n", nomFichier);

        exit(1);

    }

    fwrite(&longueur, sizeof(int), 1, flot);

    for (i = 0 ; i < longueur ; i++){

        file = file -> suiv;

        fwrite(&file -> c.numCandid, sizeof(int), 1, flot);

        fwrite(file -> c.nom, sizeof(char), 30, flot);

        fwrite(file -> c.prenom, sizeof(char), 30, flot);

        fwrite(file -> c.notes, sizeof(float), 4, flot);

        longueur2 = longueurListeChoix(file -> c.choix);

        fwrite(&longueur2, sizeof(int), 1, flot);

        for (j = 0 ; j < longueur2 ; j++){

            fwrite(file -> c.choix -> c.ville, sizeof(char), 30, flot);

            fwrite(file -> c.choix -> c.departement, sizeof(char), 30, flot);

            fwrite(&file -> c.choix -> c.decision, sizeof(int), 1, flot);

            fwrite(&file -> c.choix -> c.validation, sizeof(int), 1, flot);

            file -> c.choix = file -> c.choix -> suiv;

        }

    }

    libererFile(file);

    fclose(flot);

}

/**
 * \brief Libère en entier la file
 * \param[in] file File à libérer
 * \param[in] departement Département
 * \return void
 * 
 * Fonction qui libère en entier la file entrée en paramètres. Elle ne retourne rien.
 */

void libererFile(File file){

    if (file == NULL || file -> c.choix == NULL){

        return;
    }

    MaillonFile *aux;

    int i, longueur = longueurFile(file);

    if (longueur == 0) {

        return;

    }

    for (i = 0 ; i < longueur ; i ++){

        aux = file -> suiv;

        file -> suiv = aux -> suiv;

        free(aux);

    }

}

/**
 * \brief Charge un fichier binaire dans une file
 * \param[in] file File
 * \param[in] nomFichier Nom du fichier
 * \return File
 * 
 * Fonction qui charge un fichier binaire dans une file. Elle quitte le programme si il y a un problème d'ouverture, sinon elle retourne la file.
 */

File chargerBinFile(File file, char* nomFichier){

    Candidat c;

    FILE *flot;

    int longueur;

    flot = fopen(nomFichier ,"rb");

    if (flot == NULL){

        printf("Problème ouverture fichier [%s].\n", nomFichier);

        exit(1);

    }

    fread(&longueur, sizeof(int), 1, flot);

    c = lireBinCandidat(flot);

    while(!feof(flot)){

        file = adjonctionEnQueueFile(file, c);

        c = lireBinCandidat(flot);

    }

    fclose(flot);

    printf("\033[1;32m"); // Vert

    printf("-------------------------------\n");

    printf("Chargement fichier [%s] terminé.\n",nomFichier);

    printf("\033[0m"); // Reset couleur

    return file;

}

/**
 * \brief Charge un fichier binaire dans une liste de candidats
 * \param[in] liste Liste de candidats
 * \param[in] nomFichier Nom du fichier
 * \param[in, out] tlog Taille de la liste
 * \return ListeCandidats
 * 
 * Fonction qui charge un fichier binaire dans une liste de candidats. Elle quitte le programme si il y a un problème d'ouverture, sinon elle retourne la liste de candidats.
 */

ListeCandidats chargerBinListe(ListeCandidats liste, char* nomFichier, int *tlog){

    Candidat c;

    FILE *flot;

    flot = fopen(nomFichier ,"rb");

    if (flot == NULL){

        printf("Problème ouverture fichier [%s].\n", nomFichier);

        exit(1);

    }

    fread(tlog, sizeof(int), 1, flot);

    c = lireBinCandidat(flot);

    while(!feof(flot)){

        liste = insererCandidats(liste, c);

        c = lireBinCandidat(flot);

    }

    fclose(flot);

    printf("\033[1;32m"); // Vert

    printf("-------------------------------\n");

    printf("Chargement fichier [%s] terminé.\n", nomFichier);

    printf("\033[0m"); // Reset couleur

    return liste;

}

/**
 * \brief Insère un candidat en tête de liste
 * \param[in] l Liste de candidats
 * \param[in] c Candidat
 * \return ListeCandidats
 * 
 * Fonction qui insère un candidat en tête de liste. Elle quitte le programme si il y a un problème malloc, sinon elle retourne la liste de candidats.
 */

ListeCandidats insererEnTeteListeCandidats(ListeCandidats l, Candidat c){

	MaillonCandidat *m;

	m = (MaillonCandidat *)malloc(sizeof(MaillonCandidat));

	if (m == NULL)

	{

		printf("Problème malloc\n");

		exit(1);

	}

	m -> c = c;

	m -> suiv = l;

	return m;

}

/**
 * \brief Insère un candidat dans une liste
 * \param[in] l Liste de candidats
 * \param[in] c Candidat
 * \return ListeCandidats
 * 
 * Fonction qui insère un candidat dans une liste. Elle retourne la liste de candidats.
 */

ListeCandidats insererCandidats(ListeCandidats l, Candidat c){

    if (l == NULL)

		return insererEnTeteListeCandidats(l, c);

	if (c.numCandid < l -> c.numCandid)

		return insererEnTeteListeCandidats(l, c);

	if (c.numCandid == l -> c.numCandid)

		return l;

	l -> suiv = insererCandidats(l->suiv, c);

	return l;

}

/**
 * \brief Valide 1 voeux d'un candidat
 * \param[in] liste Liste de candidats
 * \param[in] ville Nom de la ville
 * \param[in] departement Nom du département
 * \param[in] decision Décision du département
 * \return ListeChoix
 * 
 * Fonction qui valide 1 voeux d'un candidat. Elle est liée à la fonction validerVoeux. Elle retourne la nouvelle liste des choix.
 */

ListeChoix validationVoeux(ListeChoix liste, char *ville, char *departement, int decision){

    ListeChoix aux;

    aux = liste;

    while (liste != NULL){

        if (strcmp(ville, liste -> c.ville) == 0 && strcmp(departement, liste -> c.departement) == 0){

            liste -> c.validation = decision;

            return aux;

        }

        liste = liste -> suiv;

    }

    return aux;

}

/**
 * \brief Valide 1 voeux d'un candidat
 * \param[in] liste Liste de candidats
 * \param[in] ville Nom de la ville
 * \param[in] departement Nom du département
 * \param[in, out] admis Liste des candidats admis
 * \param[in, out] attente File d'attente
 * \param[in] numCandid Numéro du candidat
 * \return ListeChoix
 * 
 * Fonction qui valide 1 voeux d'un candidat. Elle est liée à la fonction validationVoeux. Elle retourne la nouvelle liste des choix.
 */

ListeChoix validerVoeux(ListeChoix liste, char *ville, char *departement, ListeCandidats *admis, File *attente, int numCandid){

    ListeChoix aux;

    aux = liste;

    while (liste != NULL){

        if (strcmp(ville, liste -> c.ville) == 0 && strcmp(departement, liste -> c.departement) == 0)

            liste = validationVoeux(liste, liste -> c.ville, liste -> c.departement, 1);

        else {

            if (liste -> c.decision == 1 && strcmp(liste -> c.ville, "Clermont-Ferrand") == 0 && strcmp(liste -> c.departement, "Informatique") == 0){

                *admis = supprimerCandidats(*admis, numCandid);

                *admis = insererCandidats(*admis, teteFile(*attente));

                if (longueurFile(*attente) > 0)

                    *attente = suppressionEnTeteFile(*attente);
                
            }

            liste = validationVoeux(liste, liste -> c.ville, liste -> c.departement, -1);
            
        }

        liste = liste -> suiv;

    }

    return aux;

}

/**
 * \brief Divise une liste en deux
 * \param[in] liste Liste de candidats
 * \return ListeCandidats
 * 
 * Fonction qui divise une liste en deux. Elle est liée à la fonction triFusion. Elle retourne la liste de candidats.
 */

ListeCandidats diviserFusion(ListeCandidats liste){

    ListeCandidats sousListe = liste, sousListe2 = liste;

    while (sousListe->suiv && sousListe->suiv->suiv){

        sousListe = sousListe->suiv->suiv;

        sousListe2 = sousListe2->suiv;
    
    }
    
    ListeCandidats temp = sousListe2->suiv;

    sousListe2->suiv = NULL;
    
    return temp;

}

/**
 * \brief Fusion des deux sous-listes
 * \param[in] liste Sous-liste 1
 * \param[in] liste2 Sous-liste 2
 * \return ListeCandidats
 * 
 * Fonction qui fusionne les deux sous-listes du tri par fusion. Elle est liée à la fonction triFusion. Elle retourne la liste de candidats.
 */

ListeCandidats fusionListes(ListeCandidats liste, ListeCandidats liste2){

    if (liste == NULL) return liste2;
 
    if (liste2 == NULL) return liste;

    //Si même nom, compare prénom
    if (strcmp(liste->c.nom, liste2->c.nom) == 0){
        
        if(strcmp(liste->c.prenom, liste2->c.prenom) < 0){

            liste->suiv = fusionListes (liste->suiv, liste2);

            return liste;

        }
    
    }

    if (strcmp(liste->c.nom, liste2->c.nom) < 0){

        liste->suiv = fusionListes(liste->suiv, liste2);

        return liste;
    
    }

    else {

        liste2->suiv = fusionListes(liste, liste2->suiv);

        return liste2;

    }

}

/**
 * \brief Tri fusion
 * \param[in] liste Liste de candidats à trier
 * \return ListeCandidats
 * 
 * Fonction qui fait un tri par fusion pour la liste de candidats. Elle retourne la liste de candidats triée.
 */

ListeCandidats triFusion(ListeCandidats liste){

    if (liste->suiv == NULL)

        return liste;
 
    ListeCandidats liste2 = diviserFusion(liste);

    liste = triFusion(liste);

    liste2 = triFusion(liste2);
 
    return fusionListes(liste, liste2);
}

/**
 * \brief Recherche 1 candidat puis 1 voeux en particulier
 * \param[in] liste Liste de candidats
 * \param[in] nom Nom du candidat
 * \param[in] prenom Prénom du candidat
 * \param[in, out] trouveCandidat Si le candidat est trouvé
 * \param[in, out] trouveVoeu Si le voeux est trouvé
 * \param[in] ville Ville
 * \param[in] departement Département
 * \return int
 * 
 * Fonction qui recherche 1 candidat puis 1 voeux entrés en paramètres. Elle retourne 0 si le candidat n'existe pas, sinon elle retourne la décision du département.
 */

int rechercheCandidatAdmis(ListeCandidats liste, char *nom, char *prenom, int *trouveCandidat, int *trouveVoeu, char *ville, char *departement){

    if (liste == NULL){

        *trouveCandidat = 0;

        return 0;

    }

    if (strcmp(liste -> c.nom, nom) == 0 && strcmp(liste -> c.prenom, prenom) == 0){

        *trouveCandidat = 1;

        return recherche1Voeu(liste -> c.choix, ville, departement, trouveVoeu);

    }

    return rechercheCandidatAdmis(liste -> suiv, nom, prenom, trouveCandidat, trouveVoeu, ville, departement);

}

/**
 * \brief Recherche 1 voeux dans une liste de choix
 * \param[in] liste Liste
 * \param[in] ville Ville
 * \param[in] departement Département
 * \param[in, out] trouveVoeu Si le voeux est trouvé ou non
 * \return int
 * 
 * Fonction qui recherche 1 voeux dans une liste de choix. Elle retourne 0 si le voeux n'existe pas, sinon elle retourne la décision du département.
 */

int recherche1Voeu(ListeChoix liste, char *ville, char *departement, int *trouveVoeu){

    if (liste == NULL){

        *trouveVoeu = 0;

        return 0;

    }

    if (strcmp(liste -> c.ville, ville) == 0 && strcmp(liste -> c.departement, departement) == 0){

        *trouveVoeu = 1;

        return liste -> c.decision;

    }

    return recherche1Voeu(liste -> suiv, ville, departement, trouveVoeu);

}