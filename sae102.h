/** 	
	* \file sae102.h
	* \author Erwan. M & Corentin. L
	* \date 11/12/22 
	* \brief Résumé : Fichier .h regroupant les fonctions utilisées dans SAE2.c 
*/

#include <stdlib.h>

#include <stdio.h>

#include <string.h>

#include <stdbool.h>

typedef struct maillon{

	char departement[32];

	int nbP;

	char nomResp[22];

	struct maillon *suiv;

}MaillonDept, *ListeDept;

typedef struct{

	char ville[30];

	ListeDept ldept;

}VilleIUT;

typedef struct maillonNotes{

	float  v;

	struct maillonNotes *suiv;

}MaillonNotes, *ListeNotes;

typedef struct{

	char ville[30];

	char departement[30];

	int decision;

	int validation;

}Choix;

typedef struct maillonChoix{

	Choix c;

	struct maillonChoix *suiv;

}MaillonChoix, *ListeChoix;

typedef struct{

	int numCandid;

	char nom[30];

	char prenom[30];

	float notes[4];

	ListeChoix choix;

}Candidat;

typedef struct maillonCandidat{

	Candidat c;
	
	struct maillonCandidat *suiv;

}MaillonCandidat, *ListeCandidats;

typedef struct maillonFile{

	Candidat c;

	struct maillonFile *suiv;

}MaillonFile, *File;

void menuPrincipal(void);

void sousMenuAdministrateur(VilleIUT **tiut, int tlog, int *etatPhase, ListeCandidats liste);

void switchAdministrateur(int choix, VilleIUT** tiut, int tlog, int *etatPhase, ListeCandidats liste);

int fonctionChoix(void);

void affichageSousMenuUtilisateur(void);

void switchUtilisateur(int choix, VilleIUT **tiut, int tlog, ListeCandidats liste, ListeCandidats *admis, File *attente, int *etatPhase);

void sousMenuUtilisateur(VilleIUT **tiut, int tlog, ListeCandidats liste, ListeCandidats *admis, File *attente, int *etatPhase);

void clear(void);

int phaseCandid(void);

void switchPhase(int *etat);

void modifFichierPhase(int etat);

void rechercheDeptTousCandidat(ListeCandidats liste, char* nomVille, char* nomDep);

ListeDept inserer(ListeDept l, char* departement, int nbP, char* nomResp);

ListeDept insererEnTete(ListeDept l, char* departement, int nbP, char* nomResp);

int saisieNbPlaces(int *nbP);

void modifResp(VilleIUT* tiut[], int positionTab, int position, char* nomResp);

void sousMenuResponsable(ListeCandidats liste, ListeCandidats *admis, File *attente);

void affichageSousMenuResponsable(void);

void switchResponsable(int choix, ListeCandidats liste, ListeCandidats *admis, File *attente);

int rechercheCandidat(ListeCandidats liste, char* nom, char* prenom, int *trouve);

void affichageMenuPrincipal(void);

void affichageMenuPrincipal2(void);

void switchPrincipal(int choix, VilleIUT **tiut, int tlog, int *etatPhase, ListeCandidats liste, ListeCandidats *admis, File *attente);

void affichageErreurSaisie(void);

void affichageMenuAdministrateur(void);

void affichageErreurMdp(void);

int saisieNvResp(char resp[]);

void attributionNote(ListeCandidats liste, int tlog);

int ecritureNoteMoyenne(ListeCandidats liste, int tlog);

int saisieDept(char departement[]);

int saisieNbPlace(void);

ListeDept supprimer(ListeDept l, char* departement);

ListeDept supprimerEnTete(ListeDept l);

int chargerFichier(VilleIUT *tiut[], int tmax);

VilleIUT lireVille(FILE* flot);

void afficher(VilleIUT* tiut[], int tlog);

int saisieVille(char* ville, VilleIUT** tiut, int tlog);

int insertionVille(VilleIUT* tiut[], VilleIUT vIUT, int tlog);

int rechercheVille(VilleIUT** tiut, char ville[], int tlog, int *trouve);

void afficherListe(ListeDept l);

void decalageADroite(VilleIUT *tiut[], int tlog, int index);

void sauvegarderListe(ListeDept l, FILE* flot, char* ville);

int rechercheDepartement(VilleIUT* tiut[], char ville[], char departement[], int tlog, int *trouve);

int rechercheDept(ListeDept l, char departement[], int *trouve, int compteur);

void modifNbPlaces(VilleIUT* tiut[], int positionTab, int position, int nbP);

void afficherVillesIUT(VilleIUT *tiut[], int tlog);

void afficherDepartementsIUT(VilleIUT *tiut[], int pos);

void afficherNbPlaces(VilleIUT **tiut, int tlog, char* ville, char* departement);

void nbPDepartement(VilleIUT** tiut, int tlog);

void rechercheTousDepartement(VilleIUT** tiut, int tlog, char* departement, int *trouve);

ListeCandidats chargerBinCandidats(ListeCandidats liste, int *tlog);

ListeCandidats chargerTxtCandidats(ListeCandidats liste, int *tlog);

int lectureMoyennes(int tabNumCandidat[30], float tabMoyennes[30]);

Candidat lireBinCandidat(FILE* flot);

ListeChoix lireBinChoix(FILE* flot, int nbChoix);

Candidat lireTxtCandidat(FILE* flot);

ListeChoix lireTxtChoix(FILE* flot, int nbChoix);

ListeChoix ajouterChoix(ListeChoix liste, Choix choix);

ListeCandidats insertionCandidat(ListeCandidats liste, Candidat c);

ListeCandidats listeCandidatsNouv();

ListeChoix listeChoixNouv();

void affichageCandidats(ListeCandidats liste);

void gestionCandidats(ListeCandidats liste, int tlog, float noteMin, int maxAdmis);

void affichageChoix(ListeChoix liste);

void sauverCandidats(ListeCandidats liste, int tlog, char* nomFichier);

void sauverChoix(FILE *flot, ListeChoix liste);

int longueurListeChoix(ListeChoix liste);

void afficher1Candidat(Candidat c);

int longueurListeCandidats(ListeCandidats liste);

File filenouv(void);

File adjonctionEnQueueFile(File f, Candidat c);

File suppressionEnTeteFile(File f);

Candidat teteFile(File f);

int longueurFile(File f);

bool videFile(File f);

void afficherFile(File f);

ListeCandidats rechercheVoeux(ListeCandidats l);

void triSelection(int tabNumCandidat[30], float tabMoyennes[30], int tlog);

ListeCandidats changerDecisionAdmis(ListeCandidats listeC, int maxAdmis, int noteMin, File* attente);

int rechercheDeptCandidat(ListeChoix l);

void rechercheDeptCandidatE(ListeChoix l, char* nomVille, char* NomDep, ListeCandidats liste);

void sousMenuCandidature(Candidat c, VilleIUT** tiut, int tlog, ListeCandidats *admis, File *attente, int *etatPhase);

void affichageSousMenuCandidature(void);

void switchCandidature(int choix, Candidat c, VilleIUT** tiut, int tlog, ListeCandidats *admis, File *attente, int *etatPhase);

ListeChoix supprimerVoeuxCandidatEnTete(ListeChoix choix);

ListeChoix supprimerVoeuxCandidat(ListeChoix choix, char* ville, char* departement, int* trouve);

ListeCandidats supprimerEnTeteCandidats(ListeCandidats l);

ListeCandidats supprimerCandidats(ListeCandidats l, int numCandid);

ListeCandidats actualiseDecision(File file, ListeCandidats liste, int decision);

ListeCandidats listerCandidats(File file, ListeCandidats liste, int decision);

ListeChoix modifieDecision(ListeChoix liste, int decision);

ListeCandidats changeDecision(ListeCandidats liste, int tlog, float noteMin, int maxAdmis, ListeCandidats *admis, File *attente);

ListeCandidats refuser(ListeCandidats listeC, ListeCandidats liste, float noteMin);

ListeCandidats listerRefuse(ListeCandidats listeC, ListeCandidats liste, int decision);

Candidat plusGrand(ListeCandidats liste, int *pge);

ListeCandidats copierListeCandidats(ListeCandidats liste);

void sauverFile(File file, char* nomFichier);

File chargerBinFile(File file, char* nomFichier);

void libererFile(File file);

ListeCandidats remettreAZero(File *file, ListeCandidats listeC);

ListeChoix remetChoixZero(ListeChoix liste);

void freeFile(MaillonFile *file);

ListeChoix validationVoeux(ListeChoix liste, char *ville, char *departement, int decision);

ListeChoix validerVoeux(ListeChoix liste, char *ville, char *departement, ListeCandidats *admis, File *attente, int numCandid);

ListeCandidats diviserFusion(ListeCandidats liste);

ListeCandidats fusionListes(ListeCandidats liste, ListeCandidats liste2);

ListeCandidats triFusion(ListeCandidats liste);

void affichageCandidatAdmis(Candidat c, int decision);

int rechercheCandidatVoeu(ListeChoix liste, char* ville, char* departement, int* validation, int *decision);

ListeCandidats insererCandidats(ListeCandidats l, Candidat c);

ListeCandidats supprimerCandidats(ListeCandidats l, int numCandid);

ListeCandidats insererEnTeteListeCandidats(ListeCandidats l, Candidat c);

ListeCandidats chargerBinListe(ListeCandidats liste, char* nomFichier, int *tlog);

ListeCandidats listerListeCandidats(ListeCandidats listeAdmis, ListeCandidats liste, int decision);

ListeCandidats actualiseDecisionListe(ListeCandidats listeAdmis, ListeCandidats liste, int decision);

void freeListe(ListeCandidats liste);

ListeCandidats remettreAZeroListe(ListeCandidats *listeAdmis, ListeCandidats listeC);

int rechercheCandidatAdmis(ListeCandidats liste, char *nom, char *prenom, int *trouveCandidat, int *trouveVoeu, char *ville, char *departement);

int recherche1Voeu(ListeChoix liste, char *ville, char *departement, int *trouveVoeu);