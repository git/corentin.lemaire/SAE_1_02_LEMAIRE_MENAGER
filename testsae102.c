/** 	
	* \file sae102.h
	* \author Erwan. M & Corentin. L
	* \date 12/12/23 
	* \brief Résumé : Fichier .c regroupant la fonction main pour lancer le programme et les tests
*/

#include "sae102.h"

void testRechDept(void){

	VilleIUT *tiut[30];

	int tlog, pos, trouve;

	tlog = chargerFichier(tiut, 30);

	pos = rechercheDepartement(tiut, "Clermont-Ferrand\0", "Biologie\0", tlog, &trouve);

	if (trouve == 1)

		printf("Trouvé, position : %d\n", pos);

	else

		printf("Introuvable ! Position pour insérer : %d\n", pos);

}

void testPartDeux(void){

	ListeCandidats liste;
	
	liste = listeCandidatsNouv();

	int tlog;

	liste = chargerTxtCandidats(liste, &tlog);

	affichageCandidats(liste);

	sauverCandidats(liste, tlog, "candidats.bin");

	liste = listeCandidatsNouv();

	liste = chargerBinCandidats(liste, &tlog);

	affichageCandidats(liste);

}

void testAfficherFile(void){

	File f = filenouv();

	Candidat c, ca, can;

	Choix choix, choix2;

	strcpy(choix.ville,"MarcelVille\0");

	strcpy(choix.departement,"MARCEL\0");

	choix.decision = 0;

	choix.validation = 0;

	strcpy(choix2.ville,"MarcelCity\0");

	strcpy(choix2.departement,"CEL\0");

	choix2.decision = 0;

	choix2.validation = 0;




	c.numCandid = 1;

	strcpy(c.nom, "Marcel\0");

	strcpy(c.prenom, "EMarcel\0");

	c.notes[0] = 15; 

	c.notes[1] = 12; 

	c.notes[2] = 9; 

	c.notes[3] = 19; 

	c.choix = listeChoixNouv();

	c.choix = ajouterChoix(c.choix, choix);


	ca.numCandid = 2;

	strcpy(ca.nom, "Mirouge\0");

	strcpy(ca.prenom, "tel\0");

	ca.notes[0] = 1; 

	ca.notes[1] = 2; 

	ca.notes[2] = 91; 

	ca.notes[3] = 9; 

	ca.choix = listeChoixNouv();

	ca.choix = ajouterChoix(ca.choix, choix2);

	ca.choix = ajouterChoix(ca.choix, choix);

	ca.choix = ajouterChoix(ca.choix, choix2);

	ca.choix = ajouterChoix(ca.choix, choix);


	can.numCandid = 3;

	strcpy(can.nom, "Pierre\0");

	strcpy(can.prenom, "Maredesel\0");

	can.notes[0] = 17; 

	can.notes[1] = 10; 

	can.notes[2] = 11; 

	can.notes[3] = 12; 

	can.choix = listeChoixNouv();

	can.choix = ajouterChoix(can.choix, choix2);

	can.choix = ajouterChoix(can.choix, choix2);

	afficher1Candidat(c);

	afficher1Candidat(ca);

	afficher1Candidat(can);


	f = adjonctionEnQueueFile(f, c);

	f = adjonctionEnQueueFile(f, ca);

	f = adjonctionEnQueueFile(f, can);

	afficherFile(f);

}

int main(void){

	//testPartDeux();

	//testRechDept();

	//testAfficherFile();

	menuPrincipal();

}